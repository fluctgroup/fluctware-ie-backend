using Library.EventBus.Events;

namespace Identity.API.Application.IntegrationEvents.Events
{
    public class SendEmailIntegrationEvent : IntegrationEvent
    {
        public SendEmailIntegrationEvent(string sendToEmail,
            string emailSubject, string userName, string emailTemplatePath, string viewModel)
        {
            SendToEmail = sendToEmail;
            EmailSubject = emailSubject;
            UserName = userName;
            EmailTemplatePath = emailTemplatePath;
            ViewModel = viewModel;
        }

        public string SendToEmail { get; }
        public string EmailSubject { get; }
        public string UserName { get; }
        public string EmailTemplatePath { get; set; }
        public string ViewModel { get; set; }
    }
}