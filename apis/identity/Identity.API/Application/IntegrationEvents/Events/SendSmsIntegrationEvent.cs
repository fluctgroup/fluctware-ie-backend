using Library.EventBus.Events;

namespace Identity.API.Application.IntegrationEvents.Events
{
    public class SendSmsIntegrationEvent : IntegrationEvent
    {
        public SendSmsIntegrationEvent(string phoneNumber, string message)
        {
            PhoneNumber = phoneNumber;
            Message = message;
        }

        public string PhoneNumber { get; }
        public string Message { get; }
    }
}