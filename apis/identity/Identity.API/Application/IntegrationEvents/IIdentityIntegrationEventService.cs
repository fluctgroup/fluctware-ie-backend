using System.Threading.Tasks;
using Library.EventBus.Events;

namespace Identity.API.Application.IntegrationEvents
{
    public interface IIdentityIntegrationEventService
    {
        Task PublishThroughEventBusAsync(IntegrationEvent evt);
        Task SaveEventAndIdentityContextChangesAsync(IntegrationEvent evt);
    }
}