using System;
using System.Data.Common;
using System.Threading.Tasks;
using Identity.API.Data;
using Library.EventBus.Abstractions;
using Library.EventBus.Events;
using Library.IntegrationEventLog.Services;
using Library.IntegrationEventLog.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Identity.API.Application.IntegrationEvents
{
    public class IdentityIntegrationEventService : IIdentityIntegrationEventService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IEventBus _eventBus;
        private readonly IIntegrationEventLogService _eventLogService;

        public IdentityIntegrationEventService(IEventBus eventBus, ApplicationDbContext applicationDbContext,
            Func<DbConnection, IIntegrationEventLogService> integrationEventLogServiceFactory)
        {
            _applicationDbContext =
                applicationDbContext ?? throw new ArgumentNullException(nameof(applicationDbContext));
            var integrationEventLogServiceFactory1 = integrationEventLogServiceFactory ??
                                                     throw new ArgumentNullException(
                                                         nameof(integrationEventLogServiceFactory));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _eventLogService = integrationEventLogServiceFactory1(_applicationDbContext.Database.GetDbConnection());
        }

        public async Task PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            await SaveEventAndIdentityContextChangesAsync(evt);
            _eventBus.Publish(evt);
            await _eventLogService.MarkEventAsPublishedAsync(evt);
        }

        public async Task SaveEventAndIdentityContextChangesAsync(IntegrationEvent evt)
        {
            //Use of an EF Core resiliency strategy when using multiple DbContexts within an explicit BeginTransaction():
            //See: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency            
            await ResilientTransaction.New(_applicationDbContext)
                .ExecuteAsync(async () =>
                {
                    // Achieving atomicity between original Warehouse database operation and the IntegrationEventLog thanks to a local transaction
                    await _applicationDbContext.SaveChangesAsync();
                    await _eventLogService.SaveEventAsync(evt,
                        _applicationDbContext.Database.CurrentTransaction.GetDbTransaction());
                });
        }
    }
}