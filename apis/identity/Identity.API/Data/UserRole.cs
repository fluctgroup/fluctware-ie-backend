namespace Identity.API.Data
{
    public static class UserRole
    {
        public static string Tester = nameof(Tester).ToLowerInvariant();
        public static string SuperAdmin = nameof(SuperAdmin).ToLowerInvariant();
        public static string Partner = nameof(Partner).ToLowerInvariant();
    }
}