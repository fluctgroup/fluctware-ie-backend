using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Identity.API.Configuration;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Identity.API.Data
{
    public class ConfigurationDbContextSeed
    {
        public async Task SeedAsync(ConfigurationDbContext context, IConfiguration configuration)
        {
            var isDev = configuration.GetValue<string>("Environment") == "Development";

            //callbacks urls from config:
            var clientUrls = new Dictionary<string, string>();

            clientUrls.Add("WmsSpa", configuration.GetValue<string>("WmsSpa"));
            if (!context.Clients.Any())
            {
                foreach (var client in Config.GetClients(clientUrls,isDev)) context.Clients.Add(client.ToEntity());

                if (configuration.GetValue<bool>("UseFunctionalTest"))
                    foreach (var client in Config.GetFunctionalTestClients(clientUrls))
                        context.Clients.Add(client.ToEntity());

                await context.SaveChangesAsync();
            }
            // Checking always for old redirects to fix existing deployments
            // to use new swagger-ui redirect uri as of v3.0.0
            // There should be no problem for new ones
            // ref: https://github.com/dotnet-architecture/eShopOnContainers/issues/586
            else
            {
                var oldRedirects =
                    (await context.Clients.Include(c => c.RedirectUris).ToListAsync())
                    .SelectMany(c => c.RedirectUris)
                    .Where(ru => ru.RedirectUri.EndsWith("/o2c.html"))
                    .ToList();

                if (oldRedirects.Any())
                {
                    foreach (var ru in oldRedirects)
                    {
                        ru.RedirectUri = ru.RedirectUri.Replace("/o2c.html", "/oauth2-redirect.html");
                        context.Update(ru.Client);
                    }

                    await context.SaveChangesAsync();
                }
            }

            if (!context.IdentityResources.Any())
            {
                foreach (var resource in Config.GetResources()) context.IdentityResources.Add(resource.ToEntity());

                await context.SaveChangesAsync();
            }

            if (!context.ApiResources.Any())
            {
                foreach (var api in Config.GetApis()) context.ApiResources.Add(api.ToEntity());

                await context.SaveChangesAsync();
            }
            if (!context.ApiScopes.Any())
            {
                foreach (var api in Config.GetScopes()) context.ApiScopes.Add(api.ToEntity());

                await context.SaveChangesAsync();
            }
        }
    }
}