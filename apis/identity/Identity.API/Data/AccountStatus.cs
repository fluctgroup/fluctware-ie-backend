namespace Identity.API.Data
{
    public static class AccountStatus
    {
        public static string Active = nameof(Active).ToLowerInvariant();
        public static string Disabled = nameof(Disabled).ToLowerInvariant();
        public static string Blocked = nameof(Blocked).ToLowerInvariant();
        public static string WaitingValidation = nameof(WaitingValidation).ToLowerInvariant();
        public static string NeedToChangePassword = nameof(NeedToChangePassword).ToLowerInvariant();
    }
}