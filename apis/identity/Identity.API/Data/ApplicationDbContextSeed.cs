using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Identity.API.Application.IntegrationEvents.Events;
using Identity.API.Models;
using Library.Common.Extensions;
using Library.EventBus.Abstractions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

// ReSharper disable StringLiteralTypo

namespace Identity.API.Data
{
    public class ApplicationDbContextSeed
    {
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher = new PasswordHasher<ApplicationUser>();
        private AppSettings _appSettings;
        private IEventBus _eventBus;

        public async Task SeedAsync(ApplicationDbContext context, IWebHostEnvironment env,
            ILogger<ApplicationDbContextSeed> logger, IOptions<AppSettings> settings, IEventBus eventBus,
            int? retry = 0)
        {
            _eventBus = eventBus;
            _appSettings = settings.Value;
            if (retry != null)
            {
                var retryForAvailability = retry.Value;

                try
                {
                    var useCustomizationData = settings.Value.UseCustomizationData;
                    var contentRootPath = env.ContentRootPath;
                    var webroot = env.WebRootPath;

                    if (!context.Roles.Any())
                    {
                        context.Roles.AddRange(useCustomizationData
                            ? GetRolesFromFile(contentRootPath, logger)
                            : GetDefaultRole());

                        await context.SaveChangesAsync();
                    }

                    if (!context.Users.Any())
                    {
                        var users = GetUsersFromFile(contentRootPath, logger);

                        context.Users.AddRange(users.Select(x => x.Item1).ToList());
                        await context.SaveChangesAsync();
                    }

                    if (!context.UserRoles.Any())
                    {
                        context.UserRoles.AddRange(useCustomizationData
                            ? GetUserRolesFromFile(contentRootPath, logger, context)
                            : GetDefaultUserRole(context));

                        await context.SaveChangesAsync();
                        var role = context.Roles.SingleOrDefault(x => x.Name == "partner");
                        var us = context.UserRoles.Where(x=>x.RoleId==role.Id).ToList();
                        foreach (var u in us)
                        {
                            var usr = context.Users.SingleOrDefault(x => x.Id == u.UserId);
                            var acc = new CreateAccountIntegrationEvent(usr.PhoneNumber, 
                                usr.Id, usr.Email, usr.UserName);
                            _eventBus.Publish(acc);
                        }
                    }

                    if (useCustomizationData) GetPreconfiguredImages(contentRootPath, webroot, logger);
                }
                catch (Exception ex)
                {
                    if (retryForAvailability < 10)
                    {
                        retryForAvailability++;

                        logger.LogError(ex.Message, "There is an error migrating data for ApplicationDbContext");

                        await SeedAsync(context, env, logger, settings, eventBus, retryForAvailability);
                    }
                }
            }
        }

        private IEnumerable<(ApplicationUser, string)> GetUsersFromFile(string contentRootPath, ILogger logger)
        {
            var csvFileUsers = Path.Combine(contentRootPath, "Setup", "Users.csv");

            string[] csvheaders;
            try
            {
                string[] requiredHeaders =
                {
                    "firstname", "surname", "email", "phonenumber", "password", "gender",
                    "birthdate", "birthplace", "status", "nationality", "identitycardnumber", "identitycardexpirydate",
                    "taxidentificationnumber", "taxidentificationnumberexpirydate", "invitedby", "imageattachmenturi",
                    "identitycardattachmenturi",
                    "taxidentificationnumberattachmenturi", "pin", "balance"
                };
                csvheaders = GetHeaders(requiredHeaders, csvFileUsers);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw new Exception("Something Goes wrong");
            }

            var users = File.ReadAllLines(csvFileUsers)
                .Skip(1) // skip header column
                .Select(row => Regex.Split(row, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
                .SelectTry(column => CreateApplicationUser(column, csvheaders))
                .OnCaughtException(ex =>
                {
                    logger.LogError(ex.Message);
                    return (null, "");
                })
                .Where(x => x != (null, ""))
                .ToList();

            return users;
        }

        private IEnumerable<IdentityRole> GetRolesFromFile(string contentRootPath, ILogger logger)
        {
            var csvFileUsers = Path.Combine(contentRootPath, "Setup", "Roles.csv");

            if (!File.Exists(csvFileUsers)) return GetDefaultRole();

            string[] csvheaders;
            try
            {
                string[] requiredHeaders =
                {
                    "role"
                };
                csvheaders = GetHeaders(requiredHeaders, csvFileUsers);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);

                return GetDefaultRole();
            }

            var roles = File.ReadAllLines(csvFileUsers)
                .Skip(1) // skip header column
                .Select(row => Regex.Split(row, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
                .SelectTry(column => CreateIdentityRole(column, csvheaders))
                .OnCaughtException(ex =>
                {
                    logger.LogError(ex.Message);
                    return null;
                })
                .Where(x => x != null)
                .ToList();

            return roles;
        }

        private IEnumerable<IdentityUserRole<string>> GetUserRolesFromFile(string contentRootPath, ILogger logger,
            ApplicationDbContext ctx)
        {
            var csvFileUsers = Path.Combine(contentRootPath, "Setup", "UserRoles.csv");

            if (!File.Exists(csvFileUsers)) return GetDefaultUserRole(ctx);

            string[] csvheaders;
            try
            {
                string[] requiredHeaders =
                {
                    "role", "username"
                };
                csvheaders = GetHeaders(requiredHeaders, csvFileUsers);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);

                return GetDefaultUserRole(ctx);
            }

            var userRoles = File.ReadAllLines(csvFileUsers)
                .Skip(1) // skip header column
                .Select(row => Regex.Split(row, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"))
                .SelectTry(column => CreateIdentityUserRole(column, csvheaders, ctx))
                .OnCaughtException(ex =>
                {
                    logger.LogError(ex.Message);
                    return null;
                })
                .Where(x => x != null)
                .ToList();

            return userRoles;
        }

        private (ApplicationUser, string) CreateApplicationUser(string[] column, string[] headers)
        {
            if (column.Count() != headers.Count())
                throw new Exception(
                    $"column count '{column.Count()}' not the same as headers count'{headers.Count()}'");

            var firstName = column[Array.IndexOf(headers, "firstname")].Trim('"').Trim();
            var surname = column[Array.IndexOf(headers, "surname")].Trim('"').Trim();
            var gender = column[Array.IndexOf(headers, "gender")].Trim('"').Trim().ToLowerInvariant();
            var birthdate = column[Array.IndexOf(headers, "birthdate")].Trim('"').Trim();
            var birthplace = column[Array.IndexOf(headers, "birthplace")].Trim('"').Trim();
            if (!DateTime.TryParse(birthdate, out var birthdates)) birthdates = new DateTime();

            var status = column[Array.IndexOf(headers, "status")].Trim('"').Trim().ToLowerInvariant();
            var nationality = column[Array.IndexOf(headers, "nationality")].Trim('"').Trim().ToLowerInvariant();
            var identityCardNumber = column[Array.IndexOf(headers, "identitycardnumber")].Trim('"').Trim();
            var identityCardExpiryDate = column[Array.IndexOf(headers, "identitycardexpirydate")].Trim('"').Trim();
            if (!DateTime.TryParse(identityCardExpiryDate, out var identityCardExpiryDates))
                identityCardExpiryDates = new DateTime();

            var taxIdentificationNumber = column[Array.IndexOf(headers, "taxidentificationnumber")].Trim('"').Trim();
            var taxIdentificationNumberExpiryDate =
                column[Array.IndexOf(headers, "taxidentificationnumberexpirydate")].Trim('"').Trim();
            if (!DateTime.TryParse(taxIdentificationNumberExpiryDate, out var taxIdentificationNumberExpiryDates))
                taxIdentificationNumberExpiryDates = new DateTime();

            var invitedBy = column[Array.IndexOf(headers, "invitedby")].Trim('"').Trim();
            var imageAttachmentUri = column[Array.IndexOf(headers, "imageattachmenturi")].Trim('"').Trim();
            var identityCardAttachmentUri =
                column[Array.IndexOf(headers, "identitycardattachmenturi")].Trim('"').Trim();
            var taxIdentificationNumberAttachmentUri =
                column[Array.IndexOf(headers, "taxidentificationnumberattachmenturi")].Trim('"').Trim();
            var email = column[Array.IndexOf(headers, "email")].Trim('"').Trim();
            var phoneNumber = column[Array.IndexOf(headers, "phonenumber")].Trim('"').Trim();
            var normalizedEmail = email.ToUpper();
            var normalizedUserName = email.ToUpper();
            var securityStamp = Guid.NewGuid().ToString("D");
            var passwordHash =
                column[Array.IndexOf(headers, "password")].Trim('"').Trim(); // Note: This is the password
            var pinHash = column[Array.IndexOf(headers, "pin")].Trim('"').Trim(); // Note: This is the pin
            var balance = column[Array.IndexOf(headers, "balance")].Trim('"').Trim();

            var joinTime = DateTime.UtcNow;
            var user = new ApplicationUser
            {
                UserName = email,
                ReferralCode = email.ToUpper(),
                Email = email,
                PhoneNumber = phoneNumber,
                FirstName = firstName,
                Surname = surname,
                Gender = gender,
                Birthdate = birthdates,
                BirthPlace = birthplace,
                Status = status,
                Nationality = nationality,
                IdentityCardNumber = identityCardNumber,
                IdentityCardExpiryDate = identityCardExpiryDates,
                TaxIdentificationNumber = taxIdentificationNumber,
                TaxIdentificationNumberExpiryDate = taxIdentificationNumberExpiryDates,
                InvitedBy = invitedBy,
                ImageAttachmentUri = imageAttachmentUri,
                IdentityCardAttachmentUri = identityCardAttachmentUri,
                TaxIdentificationNumberAttachmentUri = taxIdentificationNumberAttachmentUri,
                NormalizedEmail = normalizedEmail,
                NormalizedUserName = normalizedUserName,
                SecurityStamp = securityStamp,
                JoinTime = joinTime
            };
            if (string.IsNullOrEmpty(passwordHash)) passwordHash = Guid.NewGuid().ToString();
            if (string.IsNullOrEmpty(pinHash)) pinHash = Guid.NewGuid().ToString();

            if (email.ToLowerInvariant() == "admin@kri.com" && _appSettings.Environment=="Production")
            {
                var integrationEvent = new SendEmailIntegrationEvent("darwinyo91@gmail.com",
                    "Your Account has been created", email,
                    "/Views/Account/GeneratedUserPassword.cshtml", passwordHash);
                _eventBus.Publish(integrationEvent);
                
            }

            user.Pin = pinHash.GeneratePinHash(joinTime.ToString());
            user.PasswordHash = _passwordHasher.HashPassword(user, passwordHash);

            return (user, balance);
        }

        private IdentityRole CreateIdentityRole(string[] column, string[] headers)
        {
            if (column.Count() != headers.Count())
                throw new Exception(
                    $"column count '{column.Count()}' not the same as headers count'{headers.Count()}'");

            var roleString = column[Array.IndexOf(headers, "role")].Trim('"').Trim().ToLowerInvariant();

            var role = new IdentityRole
            {
                Id = Guid.NewGuid().ToString(),
                Name = roleString,
                NormalizedName = roleString.ToUpper()
            };

            return role;
        }

        private IdentityUserRole<string> CreateIdentityUserRole(string[] column, string[] headers,
            ApplicationDbContext ctx)
        {
            if (column.Count() != headers.Count())
                throw new Exception(
                    $"column count '{column.Count()}' not the same as headers count'{headers.Count()}'");

            var roleString = column[Array.IndexOf(headers, "role")].Trim('"').Trim().ToLowerInvariant();
            var userString = column[Array.IndexOf(headers, "username")].Trim('"').Trim();

            var roleId = ctx.Roles.First(x => x.Name == roleString).Id;
            var userId = ctx.Users.First(x => x.UserName == userString).Id;

            var userRole = new IdentityUserRole<string>
            {
                RoleId = roleId,
                UserId = userId
            };

            return userRole;
        }

        private IEnumerable<IdentityRole> GetDefaultRole()
        {
            var role = new List<IdentityRole>
            {
                new IdentityRole("Tester"),
                new IdentityRole("SuperAdmin"),
                new IdentityRole("Partner")
            };
            return role;
        }

        private IEnumerable<IdentityUserRole<string>> GetDefaultUserRole(ApplicationDbContext ctx)
        {
            var users = ctx.Users.ToList();
            var roles = ctx.Roles.ToList();
            var role = new List<IdentityUserRole<string>>
            {
                new IdentityUserRole<string>
                {
                    UserId = users[0].Id,
                    RoleId = roles[0].Id
                },
                new IdentityUserRole<string>
                {
                    UserId = users[1].Id,
                    RoleId = roles[1].Id
                }
            };
            return role;
        }


        private static string[] GetHeaders(string[] requiredHeaders, string csvfile)
        {
            var csvheaders = File.ReadLines(csvfile).First().ToLowerInvariant().Split(',');

            if (csvheaders.Count() != requiredHeaders.Count())
                throw new Exception(
                    $"requiredHeader count '{requiredHeaders.Count()}' is different then read header '{csvheaders.Count()}'");

            foreach (var requiredHeader in requiredHeaders)
                if (!csvheaders.Contains(requiredHeader))
                    throw new Exception($"does not contain required header '{requiredHeader}'");

            return csvheaders;
        }

        private static void GetPreconfiguredImages(string contentRootPath, string webroot, ILogger logger)
        {
            try
            {
                var imagesZipFile = Path.Combine(contentRootPath, "Setup", "images.zip");
                if (!File.Exists(imagesZipFile))
                {
                    logger.LogError($" zip file '{imagesZipFile}' does not exists.");
                    return;
                }

                var imagePath = Path.Combine(webroot, "images");
                var imageFiles = Directory.GetFiles(imagePath).Select(file => Path.GetFileName(file)).ToArray();

                using (var zip = ZipFile.Open(imagesZipFile, ZipArchiveMode.Read))
                {
                    foreach (var entry in zip.Entries)
                        if (imageFiles.Contains(entry.Name))
                        {
                            var destinationFilename = Path.Combine(imagePath, entry.Name);
                            if (File.Exists(destinationFilename)) File.Delete(destinationFilename);
                            entry.ExtractToFile(destinationFilename);
                        }
                        else
                        {
                            logger.LogWarning($"Skip file '{entry.Name}' in zipfile '{imagesZipFile}'");
                        }
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Exception in method GetPreconfiguredImages WebMVC. Exception Message={ex.Message}");
            }
        }
    }
}