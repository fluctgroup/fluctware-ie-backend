using System.Threading.Tasks;
using Identity.API.Infrastructure.Services;
using Identity.API.ViewModels.Account;
using Newtonsoft.Json;

namespace Identity.API.Extensions
{
  public static class EmailSenderExtensions
  {
    public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string userName, string link)
    {
      var obj = new ValidateEmailViewModel()
      {
        CallbackUrl = link
      };
      var viewModel = JsonConvert.SerializeObject(obj);
      return emailSender.SendEmailAsync(email, "Verify Email", userName,
        "/Views/Account/VerifyEmail.cshtml", viewModel);
    }
    public static Task SendEmailForgetPinAsync(this IEmailSender emailSender, string email, string userName, string otp)
    {
      return emailSender.SendEmailAsync(email, "Forget Pin Email", userName,
        "/Views/Account/ForgetPinEmail.cshtml", otp);
    }
    public static Task SendEmailForgetPasswordAsync(this IEmailSender emailSender, string email, string userName, string otp)
    {
      return emailSender.SendEmailAsync(email, "Forget Password Email", userName,
        "/Views/Account/ForgetPasswordEmail.cshtml", otp);
    }
    public static Task SendEmailGeneratedUserPasswordAsync(this IEmailSender emailSender, string email, string userName, string password)
    {
      return emailSender.SendEmailAsync(email, "Your Account has been created", userName,
        "/Views/Account/GeneratedUserPassword.cshtml", password);
    }
  }
}
