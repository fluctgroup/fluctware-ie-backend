using System.Threading.Tasks;
using Identity.API.Infrastructure.Services;

namespace Identity.API.Extensions
{
  public static class SmsSenderExtensions
  {
    public static Task SendOTPRegistrationVerificationAsync(this ISmsSender smsSender, string number, string code,string appHash)
    {
      return smsSender.SendSmsAsync(number,
        $"JANGAN MEMBERITAHU KODE RAHASIA INI KE SIAPAPUN termasuk pihak Fluctware. WASPADA TERHADAP KASUS PENIPUAN! KODE VERIFIKASI untuk VERIFIKASI AKUN: {code} : {appHash}");
    }
    public static Task SendOTPLoginVerificationAsync(this ISmsSender smsSender, string number, string code, string appHash)
    {
      return smsSender.SendSmsAsync(number,
        $"JANGAN MEMBERITAHU KODE RAHASIA INI KE SIAPAPUN termasuk pihak Fluctware. WASPADA TERHADAP KASUS PENIPUAN! KODE VERIFIKASI untuk MASUK AKUN: {code} : {appHash}");
    }
    public static Task SendOTPDeviceVerificationAsync(this ISmsSender smsSender,string ActionType, string number, string code, string appHash)
    {
      return smsSender.SendSmsAsync(number,
        $"JANGAN MEMBERITAHU KODE RAHASIA INI KE SIAPAPUN termasuk pihak Fluctware. WASPADA TERHADAP KASUS PENIPUAN! KODE VERIFIKASI untuk {ActionType.ToUpper()}: {code} : {appHash}");
    }
  }
}
