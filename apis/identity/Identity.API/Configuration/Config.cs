using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace Identity.API.Configuration
{
    public class Config
    {
        // ApiResources define the apis in your system
        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
                new ApiResource("warehouse", "Warehouse Service")
            };
        }
        public static IEnumerable<ApiScope> GetScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("warehouse", "Warehouse Service")
            };
        }
        // Identity resources are data like user ID, name, or email address of a user
        // see: http://docs.Identity.API.io/en/release/configuration/resources.html
        public static IEnumerable<IdentityResource> GetResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }
        public static ICollection<string> GetUris(bool isDev, params string[] baseUris)
        {
            ICollection<string> uris = new List<string>();
            foreach (var uri in baseUris)
            {
                if (isDev)
                    uris.Add($"http://{uri}");

                uris.Add($"https://{uri}");
            }
            return uris;

        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(Dictionary<string, string> clientsUrl, bool isDev)
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "wmsspa",
                    ClientName = "WMS SPA OpenId Client",
                    AccessTokenLifetime = 330,// 330 seconds, default 60 minutes
                    IdentityTokenLifetime = 45,
                    AllowedGrantTypes = GrantTypes.Implicit,
                    RequirePkce = true,
                    AllowOfflineAccess = true,
                    AllowAccessTokensViaBrowser = true,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    RedirectUris =GetUris(isDev,$"{clientsUrl["WmsSpa"]}/",$"{clientsUrl["WmsSpa"]}/silent-renew.html"),
                    RequireConsent = false,
                    PostLogoutRedirectUris = GetUris(isDev,$"{clientsUrl["WmsSpa"]}/"),
                    AllowedCorsOrigins = GetUris(isDev,$"{clientsUrl["WmsSpa"]}"),
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "warehouse"
                    }
                }
            };
        }

        public static IEnumerable<Client> GetFunctionalTestClients(Dictionary<string, string> clientsUrl)
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "wmsfunctionaltest",
                    ClientName = "WMS Functional Test",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes =
                    {
                        "warehouse"
                    }
                }
            };
        }
    }
}