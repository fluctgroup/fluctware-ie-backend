using System;
using Microsoft.AspNetCore.Identity;

namespace Identity.API.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string Pin { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public DateTime Birthdate { get; set; }
        public string BirthPlace { get; set; }
        public DateTime JoinTime { get; set; }
        public string Status { get; set; }
        public string Nationality { get; set; }
        public string IdentityCardNumber { get; set; }
        public DateTime IdentityCardExpiryDate { get; set; }
        public string TaxIdentificationNumber { get; set; }
        public DateTime TaxIdentificationNumberExpiryDate { get; set; }
        public string ReferralCode { get; set; }

        /// <summary>
        ///     Id Invitee
        /// </summary>
        public string InvitedBy { get; set; }

        public string ImageAttachmentUri { get; set; }
        public string IdentityCardAttachmentUri { get; set; }
        public string TaxIdentificationNumberAttachmentUri { get; set; }
    }
}