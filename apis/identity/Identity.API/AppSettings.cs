using Library.Common;

namespace Identity.API
{
    public class AppSettings : FileSettings
    {
        public string Environment { get; set; }
        public string MvcClient { get; set; }
        public string GoogleMapsAPIKey { get; set; }
    }
}