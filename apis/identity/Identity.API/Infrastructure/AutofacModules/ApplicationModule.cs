using Autofac;
using Module = Autofac.Module;

namespace Identity.API.Infrastructure.AutofacModules
{
    public class ApplicationModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

        }
    }
}