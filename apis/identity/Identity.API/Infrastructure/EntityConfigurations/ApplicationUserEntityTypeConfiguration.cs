using Identity.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Infrastructure.EntityConfigurations
{
    public class ApplicationUserEntityTypeConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            // Primary Key
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .IsRequired();

            builder.Property(x => x.Pin);

            builder.Property(x => x.FirstName)
                .HasMaxLength(200);

            builder.Property(x => x.Surname)
                .HasMaxLength(200);

            builder.Property(x => x.Gender);

            builder.Property(x => x.Birthdate);

            builder.Property(x => x.BirthPlace);

            builder.Property(x => x.JoinTime)
                .IsRequired();

            builder.Property(x => x.Status)
                .IsRequired();

            builder.Property(x => x.Nationality);

            builder.Property(x => x.IdentityCardNumber);

            builder.Property(x => x.IdentityCardExpiryDate);

            builder.Property(x => x.TaxIdentificationNumber);

            builder.Property(x => x.TaxIdentificationNumberExpiryDate);

            builder.Property(x => x.ReferralCode)
                .IsRequired();

            builder.HasIndex(x => x.ReferralCode)
                .IsUnique();

            builder.Property(x => x.InvitedBy);

            builder.Property(x => x.ImageAttachmentUri);

            builder.Property(x => x.IdentityCardAttachmentUri);

            builder.Property(x => x.TaxIdentificationNumberAttachmentUri);

            builder.Property(x => x.PhoneNumber);

            builder.HasIndex(x => x.PhoneNumber)
                .IsUnique();

            builder.Property(x => x.UserName)
                .IsRequired();

            builder.HasIndex(x => x.UserName)
                .IsUnique();

            builder.Property(x => x.Email);

            builder.HasIndex(x => x.Email);
        }
    }
}