namespace Identity.API.Infrastructure.Config
{
    public class UrlsConfig
    {
        public string Location { get; set; }

        public string MC { get; set; }

        public class LocationOperations
        {
            public static string GetLocationByUserId(string id)
            {
                return $"/api/v1/location/{id}";
            }

            public static string GetMPLocations()
            {
                return "/api/v1/location/GetMPLocations";
            }
        }

        public class MCOperations
        {
            public static string GetMCByUserId(string id)
            {
                return $"/api/v1/MobileCollector/{id}";
            }
        }
    }
}