using System.Threading.Tasks;

namespace Identity.API.Infrastructure.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string sendToEmail,
            string emailSubject, string userName, string emailTemplatePath, string viewModel);
    }
}