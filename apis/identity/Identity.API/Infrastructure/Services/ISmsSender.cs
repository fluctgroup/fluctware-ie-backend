using System.Threading.Tasks;

namespace Identity.API.Infrastructure.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}