using System.Threading.Tasks;
using Identity.API.Application.IntegrationEvents.Events;
using Library.EventBus.Abstractions;

namespace Identity.API.Infrastructure.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        private readonly IEventBus _eventBus;

        public AuthMessageSender(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public async Task SendEmailAsync(string sendToEmail,
            string emailSubject, string userName, string emailTemplatePath, string viewModel)
        {
            var integrationEvent = new SendEmailIntegrationEvent(sendToEmail, emailSubject, userName,
                emailTemplatePath, viewModel);
            _eventBus.Publish(integrationEvent);
        }

        public async Task SendSmsAsync(string number, string message)
        {
            var integrationEvent = new SendSmsIntegrationEvent(number, message);
            _eventBus.Publish(integrationEvent);
        }
    }
}