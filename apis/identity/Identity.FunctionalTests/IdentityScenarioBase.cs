using System;
using System.IO;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Identity.FunctionalTests
{
    public class IdentityScenarioBase
    {
        public async Task<TestServer> CreateServer()
        {
            var webHostBuilder = await Host.CreateDefaultBuilder()
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseTestServer()
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .UseConfiguration(new ConfigurationBuilder()
                            .AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.json")
                            .Build()
                        )
                        .ConfigureAppConfiguration((builderContext, config) => { config.AddEnvironmentVariables(); })
                        .UseStartup<IdentityTestsStartup>();
                }).StartAsync();

            var testServer = webHostBuilder.GetTestServer();
            testServer.BaseAddress = new Uri("http://localhost:5001");
            return testServer;
        }
    }
}