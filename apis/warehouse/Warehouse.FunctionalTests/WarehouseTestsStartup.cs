using Warehouse.API;
using Warehouse.FunctionalTests.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Warehouse.FunctionalTests
{
    public class WarehouseTestsStartup : Startup
    {
        public WarehouseTestsStartup(IConfiguration env) : base(env)
        {
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
        }

        protected override void ConfigureAuth(IApplicationBuilder app)
        {
            if (Configuration["isTest"] == bool.TrueString.ToLowerInvariant())
                app.UseMiddleware<AutoAuthorizeMiddleware>();
            else
                base.ConfigureAuth(app);
        }
    }
}