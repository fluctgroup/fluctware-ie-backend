using System;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using Library.Common.Infrastructure.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Retry;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Infrastructure
{
    public class WarehouseContextSeed
    {
        private IFileService _fileService;
        private WarehouseSettings _settings;

        public async Task SeedAsync(WarehouseContext context, IFileService fileService,
            IOptions<WarehouseSettings> settings, IWebHostEnvironment env, ILogger<WarehouseContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(WarehouseContextSeed));

            await policy.ExecuteAsync(async () =>
            {
                var contentRootPath = env.ContentRootPath;
                _fileService = fileService;
                _settings = settings.Value;

                using (context)
                {
                    context.Database.Migrate();

                    if (!context.TransportRequestStatus.Any())
                    {
                        context.TransportRequestStatus.AddRange(TransportRequestStatus.List());
                    }
                    if (!context.TransportTypes.Any())
                    {
                        context.TransportTypes.AddRange(TransportType.List());
                    }

                    await context.SaveChangesAsync();
                }
            });
        }

        private void ValidateFileDirExists(string filename)
        {
            var dirRoot = new DirectoryInfo(filename);
            var dirs = dirRoot.GetDirectories();
            foreach (var directoryInfo in dirs)
            {
                if (directoryInfo.GetFiles().Length > 0) continue;

                throw new Exception("File Directory Seed is Empty");
            }
        }

        private void CleanImageDirectory(string dirPath, string parentDirName)
        {
            var directory = new DirectoryInfo(dirPath);
            var dir = directory.GetDirectories().SingleOrDefault(x => x?.Name == parentDirName);

            if (dir != null && dir.Exists)
                if (dir.GetDirectories().Length > 0)
                    foreach (var fileDir in dir.GetDirectories())
                    {
                        foreach (var fileInfo in fileDir.GetFiles()) fileInfo.Delete();

                        fileDir.Delete();
                    }
        }

        private void GetPictures(string contentRootPath, string picturePath, string zipName)
        {
            var directory = new DirectoryInfo(picturePath);
            if (!directory.Exists) directory.Create();

            foreach (var file in directory.GetFiles()) file.Delete();

            var zipFileWarehouseItemPictures = Path.Combine(contentRootPath, "Setup", zipName);
            ZipFile.ExtractToDirectory(zipFileWarehouseItemPictures, picturePath);
        }

        private void UploadPicture(string imageName, string picturePath, string folderName)
        {
            var directory = new DirectoryInfo(picturePath);

            var files = directory.GetFiles();

            var file = files.SingleOrDefault(x => x.Name == imageName);

            if (_settings.CloudStorageEnabled)
                if (file != null)
                {
                    var stream = file.OpenRead();
                    var memoryStream = new MemoryStream();
                    stream.CopyTo(memoryStream);

                    _fileService.UploadFileAsync(memoryStream.ToArray(), file.Name, folderName).Wait();
                }
        }

        private void DeleteAllFilesWithinDir(string directoryPath)
        {
            var directory = new DirectoryInfo(directoryPath);
            var files = directory.GetFiles();
            foreach (var fileInfo in files) fileInfo.Delete();
        }

        private string[] GetHeaders(string csvfile, string[] requiredHeaders, string[] optionalHeaders = null)
        {
            var csvheaders = File.ReadLines(csvfile).First().ToLowerInvariant().Split(',');

            if (csvheaders.Count() < requiredHeaders.Count())
                throw new Exception(
                    $"requiredHeader count '{requiredHeaders.Count()}' is bigger then csv header count '{csvheaders.Count()}' ");

            if (optionalHeaders != null)
                if (csvheaders.Count() > requiredHeaders.Count() + optionalHeaders.Count())
                    throw new Exception(
                        $"csv header count '{csvheaders.Count()}'  is larger then required '{requiredHeaders.Count()}' and optional '{optionalHeaders.Count()}' headers count");

            foreach (var requiredHeader in requiredHeaders)
                if (!csvheaders.Contains(requiredHeader))
                    throw new Exception($"does not contain required header '{requiredHeader}'");

            return csvheaders;
        }

        private AsyncRetryPolicy CreatePolicy(ILogger<WarehouseContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>().WaitAndRetryAsync(
                retries,
                retry => TimeSpan.FromSeconds(5),
                (exception, timeSpan, retry, ctx) =>
                {
                    logger.LogTrace(
                        $"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                }
            );
        }

        private static void GetPreconfiguredImages(string contentRootPath, string webroot, ILogger logger)
        {
            try
            {
                var imagesZipFile = Path.Combine(contentRootPath, "Setup", "WarehouseImages.zip");
                if (!File.Exists(imagesZipFile))
                {
                    logger.LogError($" zip file '{imagesZipFile}' does not exists.");
                    return;
                }

                var imagePath = Path.Combine(webroot, "images");
                var imageFiles = Directory.GetFiles(imagePath).Select(file => Path.GetFileName(file)).ToArray();

                using (var zip = ZipFile.Open(imagesZipFile, ZipArchiveMode.Read))
                {
                    foreach (var entry in zip.Entries)
                        if (imageFiles.Contains(entry.Name))
                        {
                            var destinationFilename = Path.Combine(imagePath, entry.Name);
                            if (File.Exists(destinationFilename)) File.Delete(destinationFilename);
                            entry.ExtractToFile(destinationFilename);
                        }
                        else
                        {
                            logger.LogWarning($"Skip file '{entry.Name}' in zipfile '{imagesZipFile}'");
                        }
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Exception in method GetPreconfiguredImages WebMVC. Exception Message={ex.Message}");
            }
        }
    }
}