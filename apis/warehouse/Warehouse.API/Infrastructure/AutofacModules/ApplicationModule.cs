using Autofac;
using Library.Common.Infrastructure.Idempotency;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure.Idempotency;
using Warehouse.Infrastructure.Repositories;
using System.Net.Http;
using Warehouse.API.Application.Queries;
using Warehouse.Domain.AggregatesModel.ProductAggregate;
using Warehouse.API.Application.IntegrationEvents.EventHandling;
using Library.EventBus.Abstractions;
using System.Reflection;

namespace Warehouse.API.Infrastructure.AutofacModules
{
    public class ApplicationModule
    : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => c.Resolve<IHttpClientFactory>().CreateClient())
             .As<HttpClient>();

            builder.RegisterType<WarehouseQueries>()
              .As<IWarehouseQueries>()
              .InstancePerLifetimeScope();

            builder.RegisterType<ProductQueries>()
              .As<IProductQueries>()
              .InstancePerLifetimeScope();

            builder.RegisterType<WarehouseRepository>()
        .As<IWarehouseRepository>()
        .InstancePerLifetimeScope();

            builder.RegisterType<ProductRepository>()
        .As<IProductRepository>()
        .InstancePerLifetimeScope();

            builder.RegisterType<RequestManager>()
              .As<IRequestManager>()
              .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(CreateAccountIntegrationEventHandler).GetTypeInfo().Assembly)
              .AsClosedTypesOf(typeof(IIntegrationEventHandler<>));

        }
    }
}
