﻿using System;

namespace Warehouse.API.ViewModel
{
    public class WarehouseDetailViewModel
    {
        public int Id { get; set; }
        public int SpaceRequired { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BusinessHour { get => $"{Opens.ToString()} - {Closed.ToString()}"; }
        public int SpaceLeft { get; set; }
        public string Opens { get; set; }
        public string Closed { get; set; }
        public string OwnerId { get; set; }
        public string Name { get => $"Fluctware {AreaLevel3}"; }

        public string Location { get => $"{AreaLevel2},{AreaLevel3}"; }
        public string AreaLevel1 { get; set; }
        public string AreaLevel2 { get; set; }
        public string AreaLevel3 { get; set; }
        public string AreaLevel4 { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Chiller { get; set; }
        public int Freezer { get; set; }
    }
}
