﻿namespace Warehouse.API.ViewModel
{
    public class CheckWarehouseStockViewModel
    {
        public int WarehouseId { get; set; }
        public string SKUStock { get; set; }
    }
}
