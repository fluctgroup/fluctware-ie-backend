﻿namespace Warehouse.API.ViewModel
{
    public class WarehouseViewModel
    {
        public int Id { get; set; }
        public int SpaceLeft { get; set; }
        public string Opens { get; set; }
        public string Closed { get; set; }
        public string OwnerId { get; set; }
        public string Name { get; set; }
        public string AreaLevel1 { get; set; }
        public string AreaLevel2 { get; set; }
        public string AreaLevel3 { get; set; }
        public string AreaLevel4 { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Chiller { get; set; }
        public int Freezer { get; set; }
    }
}
