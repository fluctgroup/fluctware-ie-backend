﻿using System;

namespace Warehouse.API.ViewModel
{
    public class SearchWarehouseViewModel
    {
        public string AreaLevel1 { get; set; }
        public string AreaLevel2 { get; set; }
        public string AreaLevel3 { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int SpaceRequired { get; set; }
    }
}
