﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Domain.AggregatesModel.ProductAggregate;

namespace Warehouse.API.ViewModel
{
    public class PrincipleViewModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public PrincipleViewModel()
        {

        }
        public PrincipleViewModel(Principle principle)
        {
            Name = principle.Name;
            Id = principle.Id;
        }
    }
}
