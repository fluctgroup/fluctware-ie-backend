﻿using System;
using System.Collections.Generic;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.API.ViewModel
{
    public class TransportViewModel
    {
        public TransportViewModel(TransportRequest request,string from, string to)
        {
            RequestId = request.Id;
            From = from;
            FromId = int.TryParse(request.From,out var fr)? fr:0;
            To = to;
            ToId = int.TryParse(request.To, out var t) ? t : 0;
            Status = request.Status.Name;
            Type = request.Type.Name;
            Items = request.Items;
            InboundTime = request.InboundTime;
        }
        public DateTime InboundTime { get; set; }
        public int RequestId { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public int ToId { get; set; }
        public int FromId { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public IEnumerable<TransportRequestItem> Items { get; set; }
    }
}
