﻿using System;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.API.ViewModel
{
    public class InventoryViewModel
    {
        public InventoryViewModel()
        {

        }
        public InventoryViewModel(Stock stock)
        {
            SKU = stock.SKU;
            Product = new ProductViewModel(stock.Product);
            WarehouseId = stock.WarehouseId;
            ExpiryDate = stock.ExpiryDate;
            InboundDate = stock.InboundDate;
            Qty = stock.Qty;
            Note = stock.Note;
            Quantifier = stock.Quantifier;
            Unique = stock.Unique;
            AuthorId = stock.AuthorId;
            Id = stock.Id;
        }
        public int Id { get; private set; }
        public ProductViewModel Product { get; set; }
        public int WarehouseId { get; private set; }
        public DateTime ExpiryDate { get; private set; }
        public DateTime InboundDate { get; private set; }
        public int Qty { get; private set; }
        public string Note { get; private set; }
        public string SKU { get; private set; }
        public string AuthorId { get; private set; }
        public string Quantifier { get; private set; }
        public bool Unique { get; private set; }
    }
}
