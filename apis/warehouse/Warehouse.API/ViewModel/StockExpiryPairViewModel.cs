﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.API.ViewModel
{
    public class StockExpiryPairViewModel
    {
        public int StockId { get; set; }
        public DateTime ExpiryDate { get; set; }
        
        public int Amount { get; set; }
    }
}
