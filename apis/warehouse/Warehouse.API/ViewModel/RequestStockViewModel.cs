﻿using System;

namespace Warehouse.API.ViewModel
{
    public class RequestStockViewModel
    {
        public int WarehouseId { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Quantifier { get; set; }
        public string SKU { get; set; }
        public DateTime ExpiryDate { get; set; }

    }
}
