﻿using System;

namespace Warehouse.API.ViewModel
{
    public class CheckStockByExpireViewModel
    {
        public int Inquirer { get; set; }
        public bool Any { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string SKUStock { get; set; }
    }
}
