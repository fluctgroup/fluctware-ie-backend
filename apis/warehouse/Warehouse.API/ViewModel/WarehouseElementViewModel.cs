﻿namespace Warehouse.API.ViewModel
{
    public class WarehouseElementViewModel
    {
        public int Id { get; set; }
        public string OwnerId { get; set; }
        public string Name { get; set; }
        public string AreaLevel1 { get; set; }
        public string AreaLevel2 { get; set; }
        public string AreaLevel3 { get; set; }
        public string Location { get=> $"{AreaLevel2},{AreaLevel3}"; }
        public string BusinessHour { get => $"{Opens.ToString()} - {Closed.ToString()}"; }
        public string Opens { get; set; }
        public string Closed { get; set; }
        public int SpaceAvailable { get; set; }
    }
}
