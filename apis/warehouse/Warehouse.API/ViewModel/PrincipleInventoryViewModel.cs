﻿using System;
using Warehouse.Domain.AggregatesModel.ProductAggregate;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using W = Warehouse.Domain.AggregatesModel.WarehouseAggregate.Warehouse;
namespace Warehouse.API.ViewModel
{
    public class PrincipleInventoryViewModel
    {
        public PrincipleInventoryViewModel()
        {

        }
        public PrincipleInventoryViewModel(Principle principle,Stock stock,W warehouse)
        {
            PrincipleId = principle.Id;
            SKU = stock.SKU;
            WarehouseId = stock.WarehouseId;
            ExpiryDate = stock.ExpiryDate;
            InboundDate = stock.InboundDate;
            Qty = stock.Qty;
            Note = stock.Note;
            Quantifier = stock.Quantifier;
            Unique = stock.Unique;
            AuthorId = stock.AuthorId;
            Id = stock.Id;

            WarehouseName = warehouse.Name;          
            AreaLevel1 = warehouse.AreaLevel1;
            AreaLevel2 = warehouse.AreaLevel2;
            AreaLevel3 = warehouse.AreaLevel3;
            AreaLevel4 = warehouse.AreaLevel4;
            Latitude = warehouse.Latitude;
            Longitude = warehouse.Longitude;
            Name = stock.Product.Name;
            PhotoUrl = stock.Product.PhotoUrl;
        }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public int PrincipleId { get; set; }
        public int Id { get; private set; }
        public int WarehouseId { get; private set; }
        public DateTime ExpiryDate { get; private set; }
        public DateTime InboundDate { get; private set; }
        public int Qty { get; private set; }
        public string Note { get; private set; }
        public string SKU { get; private set; }
        public string AuthorId { get; private set; }
        public string Quantifier { get; private set; }
        public bool Unique { get; private set; }
        public string WarehouseName { get; private set; }
        public string AreaLevel1 { get; private set; }
        public string AreaLevel2 { get; private set; }
        public string AreaLevel3 { get; private set; }
        public string AreaLevel4 { get; private set; }
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
    }
}
