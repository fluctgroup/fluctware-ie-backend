﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Domain.AggregatesModel.ProductAggregate;

namespace Warehouse.API.ViewModel
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {

        }
        public ProductViewModel(Product product)
        {
            Id = product.Id;
            SKUPrinciple = product.SKUPrinciple;
            ExpireDate = product.ExpireDate;
            HasExpireDate = product.HasExpireDate;
            Name = product.Name;
            HasRange = product.HasRange;
            AuthorId = product.AuthorId;
            PhotoUrl = product.PhotoUrl;
            RangeHigh = product.RangeHigh;
            RangeLow = product.RangeLow;
            LowQty = product.LowQty;
            PrincipleId = product.GetPrincipleId();
            ExpireDateLimit = product.ExpireDateLimit;
        }
        public int PrincipleId { get; set; }
        public string SKUPrinciple { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool HasExpireDate { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public bool HasRange { get; set; }
        public string AuthorId { get; set; }
        public string PhotoUrl { get; set; }
        public string RangeHigh { get; set; }
        public string RangeLow { get; set; }
        public int LowQty { get; set; }
        public int ExpireDateLimit { get; set; }
    }
}
