﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.API.ViewModel
{
    public class WarehouseStockPairViewModel
    {
        public int WarehouseId { get; set; }
        public string WarehouseName { get; set; }
        public List<StockExpiryPairViewModel> Stocks { get; set; }
    }
}
