﻿using System;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.API.ViewModel
{
    public class InventoryHistoryViewModel
    {
        public InventoryHistoryViewModel()
        {

        }
        public string Note { get; set; }
        public string ProductName { get; set; }
        public int StockId { get; set; }
        public int BeforeStock { get; set; }
        public int AfterStock { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
