using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using W = Warehouse.Domain.AggregatesModel.WarehouseAggregate.Warehouse;
using Warehouse.API.ViewModel;
using Library.Common.ViewModels;
using Warehouse.Infrastructure;
using Warehouse.API.Application.Commands;
using Library.Common.Commands;
using MediatR;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.API.Controllers
{
    [Route("api/v1/[controller]")]
    //[Authorize]
    [ApiController]
    public class WarehouseController : ControllerBase
    {
        private readonly IWarehouseIntegrationEventService _warehouseIntegrationEventService;
        private readonly WarehouseContext _context;
        private readonly WarehouseSettings _settings;
        private readonly IMediator _mediator;

        public WarehouseController(WarehouseContext context, IOptionsSnapshot<WarehouseSettings> settings,
            IMediator mediator,IWarehouseIntegrationEventService warehouseIntegrationEventService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _warehouseIntegrationEventService = warehouseIntegrationEventService ??
                                              throw new ArgumentNullException(nameof(warehouseIntegrationEventService));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _settings = settings.Value;
        }
        private WarehouseViewModel Convert(W x)
        {
            return new WarehouseViewModel()
            {
                Id = x.Id,
                Name=x.Name,
                AreaLevel1 = x.AreaLevel1,
                AreaLevel2 = x.AreaLevel2,
                AreaLevel3 = x.AreaLevel3,
                AreaLevel4 = x.AreaLevel4,
                Closed = x.Closed,
                Opens = x.Opens,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                OwnerId = x.OwnerId,
                SpaceLeft = x.SpaceLeft,
                Chiller=x.Chillers,
                Freezer=x.Freezers
            };
        }
        private WarehouseDetailViewModel ConvertDetail(W x)
        {
            return new WarehouseDetailViewModel()
            {
                Id = x.Id,
                AreaLevel1 = x.AreaLevel1,
                AreaLevel2 = x.AreaLevel2,
                AreaLevel3 = x.AreaLevel3,
                AreaLevel4 = x.AreaLevel4,
                Closed = x.Closed,
                Opens = x.Opens,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                OwnerId = x.OwnerId,
                SpaceLeft = x.SpaceLeft,
                Chiller = x.Chillers,
                Freezer = x.Freezers
            };
        }
        [Route("all")]
        [HttpGet]
        [ProducesResponseType(typeof(List<WarehouseElementViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllWarehouse(CancellationToken cancellationToken = default(CancellationToken))
        {
            var w = await _context.Warehouses.ToListAsync();
            var warehouses= w.Select(x => new WarehouseElementViewModel()
            {
                Id = x.Id,
                OwnerId = x.OwnerId,
                AreaLevel1 = x.AreaLevel1,
                AreaLevel2 = x.AreaLevel2,
                AreaLevel3 = x.AreaLevel3,
                Closed = x.Closed,
                Opens = x.Opens,
                Name = x.Name,
                SpaceAvailable = x.SpaceLeft
            }).ToList();
            return Ok(warehouses);
        }
        [Route("except/{id}")]
        [HttpGet]
        [ProducesResponseType(typeof(List<WarehouseElementViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllWarehouseExcept(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            var w = await _context.Warehouses.Where(x=>x.Id!=id).ToListAsync();
            var warehouses = w.Select(x => new WarehouseElementViewModel()
            {
                Id = x.Id,
                OwnerId = x.OwnerId,
                AreaLevel1 = x.AreaLevel1,
                AreaLevel2 = x.AreaLevel2,
                AreaLevel3 = x.AreaLevel3,
                Closed = x.Closed,
                Opens = x.Opens,
                Name = x.Name,
                SpaceAvailable = x.SpaceLeft
            }).ToList();
            return Ok(warehouses);
        }
        [Route("available-owner")]
        [HttpGet]
        [ProducesResponseType(typeof(List<Owner>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllAvailableOwner (CancellationToken cancellationToken = default(CancellationToken))
        {
            var o=await _context.Warehouses.Select(x => x.OwnerId).ToListAsync();
            var w = await _context.Owners
                .Where(x=>!o.Contains(x.OwnerId))
                .ToListAsync();
            return Ok(w);
        }

        [Route("owner/{id}")]
        [HttpGet]
        [ProducesResponseType(typeof(Owner), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllAvailableOwner(string id,CancellationToken cancellationToken = default(CancellationToken))
        {
            var w = await _context.Owners.SingleOrDefaultAsync(x => x.OwnerId == id);
            return Ok(w);
        }
        [Route("user/{id}")]
        [HttpGet]
        [ProducesResponseType(typeof(WarehouseElementViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllWarehouseByUserId(string id, CancellationToken cancellationToken = default(CancellationToken))
        {
            var w = await _context.Warehouses
                .SingleOrDefaultAsync(x => x.OwnerId == id);
            if (w != null)
            {
                var warehouses = new WarehouseElementViewModel()
                {
                    Id = w.Id,
                    OwnerId = w.OwnerId,
                    AreaLevel1 = w.AreaLevel1,
                    AreaLevel2 = w.AreaLevel2,
                    AreaLevel3 = w.AreaLevel3,
                    Closed = w.Closed,
                    Opens = w.Opens,
                    Name = w.Name,
                    SpaceAvailable = w.SpaceLeft
                };
                return Ok(warehouses);
            }
            return Ok(w);
        }
        
        [Route("search")]
        [HttpPost]
        [ProducesResponseType(typeof(WarehouseDetailViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetWarehouseByArea([FromBody] SearchWarehouseViewModel search, CancellationToken cancellationToken = default(CancellationToken))
        {
            var warehouse = await _context.Warehouses
                .FirstOrDefaultAsync(x => x.AreaLevel1.Contains(search.AreaLevel1) && 
                x.AreaLevel2.Contains(search.AreaLevel2) &&
                x.AreaLevel3.Contains(search.AreaLevel3) &&
                x.SpaceLeft>search.SpaceRequired);
            WarehouseDetailViewModel result = null;
            if (warehouse != null)
            {
                result = ConvertDetail(warehouse);
                result.SpaceRequired = search.SpaceRequired;
                result.StartDate = search.StartDate;
                result.EndDate= search.EndDate;
            }
            return Ok(result);
        }

        [Route("detail/{id}")]
        [HttpGet]
        [ProducesResponseType(typeof(WarehouseViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetWarehouseById(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            var warehouse = await _context.Warehouses
                .FirstOrDefaultAsync(x => x.Id==id);
            WarehouseViewModel result = null;
            if (warehouse != null)
            {
                result = Convert(warehouse);
            }
            return Ok(result);
        }
        
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddWarehouse([FromBody] AddWarehouseCommand cmd, 
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<AddWarehouseCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(AddWarehouse), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> EditWarehouse([FromBody] UpdateWarehouseCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<UpdateWarehouseCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)Ok() :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteWarehouse([FromQuery] int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var warehouse = await _context.Warehouses.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (warehouse == null)
                {
                    return NotFound(new ValidationViewModel("server","Warehouse not found"));
                }
                _context.Warehouses.Remove(warehouse);
                await _context.SaveChangesAsync();
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }
    }
}