﻿using Library.Common.Commands;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.API.Application.Commands;
using Warehouse.API.Application.Queries;
using Warehouse.API.ViewModel;
using Warehouse.Infrastructure;

namespace Warehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryController : ControllerBase
    {

        private readonly IWarehouseIntegrationEventService _warehouseIntegrationEventService;
        private readonly IWarehouseQueries _queries;
        private readonly WarehouseContext _context;
        private readonly WarehouseSettings _settings;
        private readonly IMediator _mediator;
        public InventoryController(WarehouseContext context, IWarehouseQueries queries, IOptionsSnapshot<WarehouseSettings> settings,
            IMediator mediator, IWarehouseIntegrationEventService warehouseIntegrationEventService)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _warehouseIntegrationEventService = warehouseIntegrationEventService ??
                                              throw new ArgumentNullException(nameof(warehouseIntegrationEventService));
            _settings = settings.Value;
        }


        [Route("check-stock/by-expire")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<WarehouseStockPairViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CheckStockByExpireDate([FromBody] CheckStockByExpireViewModel vm, CancellationToken cancellationToken = default(CancellationToken))
        {

            try
            {
                var stock = await _queries.CheckStockByExpireDate(vm.Inquirer,vm.SKUStock, vm.StartDate, vm.EndDate,vm.Any, cancellationToken);

                return Ok(stock);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }

        }
        [Route("check-stock/warehouse")]
        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<StockExpiryPairViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CheckStockByExpireDateWarehouse([FromBody] CheckWarehouseStockViewModel vm, CancellationToken cancellationToken = default(CancellationToken))
        {

            try
            {
                var stock = await _queries.CheckAllWarehouseStock(vm.WarehouseId, vm.SKUStock, cancellationToken);

                return Ok(stock);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }

        }

        [HttpGet("warehouse/{id}")]
        [ProducesResponseType(typeof(IEnumerable<InventoryViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetInventoriesByWarehouseId(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var products = await _queries.InventoriesByWarehouseId(id, cancellationToken);

                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }


        [HttpGet("stock/{id}")]
        [ProducesResponseType(typeof(InventoryViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetInventoryByStockId(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var products = await _queries.InventoryById(id, cancellationToken);

                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpGet("stock/history/{id}")]
        [ProducesResponseType(typeof(List<InventoryHistoryViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetInventoryHistoryByStockId(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var logs = await _queries.InventoryHistoryById(id, cancellationToken);

                return Ok(logs);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPut("edit")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> EditStock([FromBody] EditStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<EditStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)Ok() :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }
    }
}
