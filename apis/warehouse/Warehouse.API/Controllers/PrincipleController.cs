﻿using Library.Common.Commands;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.API.Application.Commands;
using Warehouse.API.Application.Queries;
using Warehouse.API.ViewModel;
using Warehouse.Infrastructure;

namespace Warehouse.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PrincipleController : ControllerBase
    {
        private readonly IWarehouseIntegrationEventService _warehouseIntegrationEventService;
        private readonly IProductQueries _queries;
        private readonly WarehouseContext _context;
        private readonly WarehouseSettings _settings;
        private readonly IMediator _mediator;
        public PrincipleController(WarehouseContext context, IProductQueries queries, IOptionsSnapshot<WarehouseSettings> settings,
            IMediator mediator, IWarehouseIntegrationEventService warehouseIntegrationEventService)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _warehouseIntegrationEventService = warehouseIntegrationEventService ??
                                              throw new ArgumentNullException(nameof(warehouseIntegrationEventService));
            _settings = settings.Value;
        }

        [HttpPost("new")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> NewPrinciple([FromBody] AddPrincipleCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<AddPrincipleCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(NewPrinciple), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }
        [HttpPut("edit")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> EditPrinciple([FromBody] EditPrincipleCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<EditPrincipleCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)Ok() :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PrincipleViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllPrinciple(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var products = await _queries.GetAllPrinciple(cancellationToken);

                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PrincipleViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPrincipleById(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var principle = await _queries.GetPrincipleById(id,cancellationToken);

                return Ok(principle);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }


        [HttpGet("items/{id}")]
        [ProducesResponseType(typeof(IEnumerable<PrincipleInventoryViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetPrincipleItemsById(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var principle = await _queries.GetPrincipleInventoryById(id, cancellationToken);

                return Ok(principle);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }
    }
}
