﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Warehouse.API.Application.Commands;
using Warehouse.API.Application.Queries;
using Warehouse.API.ViewModel;
using Warehouse.Infrastructure;

namespace Warehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransportController : ControllerBase
    {

        private readonly IWarehouseIntegrationEventService _warehouseIntegrationEventService;
        private readonly WarehouseContext _context; 
        private readonly IWarehouseQueries _queries;
        private readonly WarehouseSettings _settings;
        private readonly IMediator _mediator;

        public TransportController(WarehouseContext context, IWarehouseQueries queries, IOptionsSnapshot<WarehouseSettings> settings,
            IMediator mediator, IWarehouseIntegrationEventService warehouseIntegrationEventService)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _warehouseIntegrationEventService = warehouseIntegrationEventService ??
                                              throw new ArgumentNullException(nameof(warehouseIntegrationEventService));
            _settings = settings.Value;
        }

        [HttpGet("history/{id}")]
        [ProducesResponseType(typeof(IEnumerable<TransportViewModel>), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetHistoryTransportByWarehouseId(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var products = await _queries.FetchHistoryTransportByWarehouse(id, cancellationToken);

                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpGet("ongoing/{id}")]
        [ProducesResponseType(typeof(IEnumerable<TransportViewModel>), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetOngoingTransportByWarehouseId(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var products = await _queries.FetchOngoingTransportByWarehouse(id, cancellationToken);

                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpGet("detail/{id}")]
        [ProducesResponseType(typeof(TransportViewModel), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetTransportDetailById(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var products = await _queries.TransportDetailById(id, cancellationToken);

                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("inbound")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InboundStock([FromBody] InboundStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<InboundStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(InboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("outbound")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> OutboundStock([FromBody] OutboundStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<OutboundStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(InboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/inbound")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> RequestInboundStock([FromBody] RequestInboundStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<RequestInboundStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/outbound")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> RequestOutboundStock([FromBody] RequestOutboundStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<RequestOutboundStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }
        [HttpPost("req/outbound/out-delivery")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> OutboundRequestedStock([FromBody] OutboundRequestedStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<OutboundRequestedStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/inbound/approve")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ApproveRequestInboundStock([FromBody] ApproveRequestInboundStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<ApproveRequestInboundStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/inbound/reject")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> RejectRequestInboundStock([FromBody] RejectRequestInboundStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<RejectRequestInboundStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/inbound/out-delivery")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> InboundRequestedStock([FromBody] InboundRequestedStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<InboundRequestedStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/outbound/approve")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ApproveRequestOutboundStock([FromBody] ApproveRequestOutboundStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<ApproveRequestOutboundStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/outbound/reject")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> RejectRequestOutboundStock([FromBody] RejectRequestOutboundStockCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<RejectRequestOutboundStockCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/return")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ReturnRequest([FromBody] ReturnRequestCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<ReturnRequestCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(RequestInboundStock), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPost("req/cancel")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CancelTransportRequest ([FromBody] CancelRequestTransportCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<CancelRequestTransportCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)Ok() :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }
    }
}
