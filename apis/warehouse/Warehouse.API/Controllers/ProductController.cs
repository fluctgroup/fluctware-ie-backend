﻿using Library.Common.Commands;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.API.Application.Commands;
using Warehouse.API.Application.Queries;
using Warehouse.API.ViewModel;
using Warehouse.Infrastructure;

namespace Warehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IWarehouseIntegrationEventService _warehouseIntegrationEventService;
        private readonly IProductQueries _queries;
        private readonly WarehouseContext _context;
        private readonly WarehouseSettings _settings;
        private readonly IMediator _mediator;
        public ProductController(WarehouseContext context,IProductQueries queries, IOptionsSnapshot<WarehouseSettings> settings,
            IMediator mediator, IWarehouseIntegrationEventService warehouseIntegrationEventService)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _queries = queries ?? throw new ArgumentNullException(nameof(queries));
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _warehouseIntegrationEventService = warehouseIntegrationEventService ??
                                              throw new ArgumentNullException(nameof(warehouseIntegrationEventService));
            _settings = settings.Value;
        }

        [HttpPost("new")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> NewProduct([FromBody] AddProductCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<AddProductCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)CreatedAtAction(nameof(NewProduct), null) :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [HttpPut("edit")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> EditProduct([FromBody] EditProductCommand cmd,
            [FromHeader(Name = "x-requestid")] string requestId)
        {
            try
            {
                ValidationViewModel commandResult = null;
                if (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                {
                    var req = new IdentifiedCommand<EditProductCommand, ValidationViewModel>(cmd, guid);
                    commandResult = await _mediator.Send(req);
                }

                return commandResult == null ?
                  (IActionResult)Ok() :
                  BadRequest(commandResult);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ProductViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllProduct(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var products=await _queries.GetAllProducts(cancellationToken);

                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [Route("detail/{id}")]
        [HttpGet]
        [ProducesResponseType(typeof(ProductViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProductById(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var products = await _queries.GetProductById(id,cancellationToken);

                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }

        [Route("{sku}")]
        [HttpGet]
        [ProducesResponseType(typeof(ProductViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ValidationViewModel), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProductBySKU(string sku, CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                ValidationViewModel commandResult = null;
                var products = await _queries.GetProductBySKU(sku, cancellationToken);
                if (products == null)
                {
                    return BadRequest(new ValidationViewModel("server", "Product tidak ditemukan"));
                }
                return Ok(products);
            }
            catch (Exception ex)
            {
                return BadRequest(new ValidationViewModel("server", ex.Message));
            }
        }
    }
}
