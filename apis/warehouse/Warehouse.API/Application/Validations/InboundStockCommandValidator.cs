using FluentValidation;
using Warehouse.API.Application.Commands;

namespace Warehouse.API.Application.Validations
{
    public class InboundStockCommandValidator : AbstractValidator<InboundStockCommand>
  {
    public InboundStockCommandValidator()
    {
      RuleFor(x=>x.WarehouseId).NotEmpty().WithMessage("No WarehouseId found");
      RuleFor(x => x.ProductId).NotEmpty().WithMessage("No ProductId found");
      RuleFor(x => x.AuthorId).NotEmpty().WithMessage("No AuthorId found");
      RuleFor(x => x.Items).NotEmpty().WithMessage("No Items found");
    }
  }
}
