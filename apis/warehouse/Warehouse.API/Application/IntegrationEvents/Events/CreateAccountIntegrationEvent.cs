using System.Collections.Generic;
using Library.EventBus.Events;

namespace Warehouse.API.Application.IntegrationEvents.Events
{
    public class CreateAccountIntegrationEvent : IntegrationEvent
    {
        public CreateAccountIntegrationEvent(string phoneNumber, string id, string email, string name)
        {
            PhoneNumber = phoneNumber;
            Email = email;
            Name = name;
            Id = id;
        }

        public string PhoneNumber { get; }
        public string Email { get; }
        public string Name { get; }
        public string Id { get; }
    }
}