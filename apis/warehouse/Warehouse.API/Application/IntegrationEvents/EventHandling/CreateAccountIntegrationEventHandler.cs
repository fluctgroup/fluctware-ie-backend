﻿using System;
using System.Threading.Tasks;
using Library.EventBus.Abstractions;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Warehouse.API.Application.IntegrationEvents.Events;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.IntegrationEvents.EventHandling
{
    public class CreateAccountIntegrationEventHandler : IIntegrationEventHandler<CreateAccountIntegrationEvent>
    {
        private readonly ILoggerFactory _logger;
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public CreateAccountIntegrationEventHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository, ILoggerFactory logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task Handle(CreateAccountIntegrationEvent @event)
        {
            try
            {
                var owner = new Owner(@event.Id, @event.Name, @event.Email, @event.PhoneNumber);
                await _context.Owners.AddAsync(owner);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.CreateLogger(nameof(CreateAccountIntegrationEventHandler))
                .LogTrace(ex.Message);
            }
        }
    }
}
