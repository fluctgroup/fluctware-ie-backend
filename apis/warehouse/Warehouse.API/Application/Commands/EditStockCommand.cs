using System;
using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
  public class EditStockCommand : IRequest<ValidationViewModel>
  {
    public EditStockCommand()
    {
    }
    public EditStockCommand(int stockId, int qty, string note, string sku,
            string authorId, DateTime expiryDate, string quantifier,
            bool unique = false) : this()
    {
            StockId = stockId;
            Unique = unique;
            Note = !string.IsNullOrWhiteSpace(note) ? note : Note;
            SKU = !string.IsNullOrWhiteSpace(sku) ? sku : SKU;
            AuthorId = !string.IsNullOrWhiteSpace(authorId) ? authorId : throw new ArgumentNullException(nameof(authorId));
            ExpiryDate = expiryDate;
            Quantifier = !string.IsNullOrWhiteSpace(quantifier) ? quantifier : throw new ArgumentNullException(nameof(quantifier));
            Qty = qty > 0 ? qty : throw new ArgumentNullException(nameof(qty));
        }
        [DataMember] public int StockId { get; set; }
        [DataMember] public DateTime ExpiryDate { get; set; }
        [DataMember] public int Qty { get; set; }
        [DataMember] public string Note { get; set; }
        [DataMember] public string SKU { get; set; }
        [DataMember] public string AuthorId { get; set; }
        [DataMember] public string Quantifier { get; set; }
        /// <summary>
        /// Unique Stock
        /// </summary>
        [DataMember] public bool Unique { get; set; }
    }
}
