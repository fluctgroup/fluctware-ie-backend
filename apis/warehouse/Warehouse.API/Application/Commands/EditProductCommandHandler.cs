using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.Infrastructure.Services;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.Commands
{
    public class EditProductCommandHandler : IRequestHandler<EditProductCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;
        private IFileService _fileService;

        public EditProductCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository, IFileService fileService)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
            _fileService = fileService;
        }

        public async Task<ValidationViewModel> Handle(EditProductCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var product = await _context.Products
                           .SingleOrDefaultAsync(x => x.Id == command.ProductId);

                    if (product == null)
                    {
                        return new ValidationViewModel()
                        {
                            Key = "server",
                            Message = "Product not found."
                        };
                    }
                    //byte[] bytes = Convert.FromBase64String(command.PhotoUrl);
                    //await _fileService.UploadFileAsync(bytes, command.PhotoUrl, "ProductImages");
                    product.Edit(command.SKUPrinciple, command.ExpireDate, command.HasExpireDate,
                        command.Name, command.HasRange, command.AuthorId, command.PhotoUrl, command.RangeHigh, 
                        command.RangeLow,command.LowQty, command.PrincipleId,command.ExpireDateLimit);
                    await _warehouseRepository.UnitOfWork.SaveEntitiesAsync();
                    return null;
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class EditProductIdentifiedCommandHandler : IdentifiedCommandHandler<EditProductCommand, ValidationViewModel>
    {
        public EditProductIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
