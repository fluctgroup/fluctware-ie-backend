using System;
using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
    public class EditPrincipleCommand : IRequest<ValidationViewModel>
    {
        public EditPrincipleCommand()
        {
        }
        public EditPrincipleCommand(int id,string name) : this()
        {
            Id = id;
            Name = name;
        }
        [DataMember] public int Id { get; set; }
        [DataMember] public string Name { get; set; }
    }
}
