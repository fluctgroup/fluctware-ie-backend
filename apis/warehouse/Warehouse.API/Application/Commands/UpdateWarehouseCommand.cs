using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
    public class UpdateWarehouseCommand : IRequest<ValidationViewModel>
    {
        public UpdateWarehouseCommand()
        {
        }
        public UpdateWarehouseCommand(int warehouseId,string ownerId, string name, int spaceLeft,
                double latitude, double longitude, string opens, string closed,
                string areaLevel1, string areaLevel2, string areaLevel3, string areaLevel4,
                int freezer, int chiller) : this()
        {
            WarehouseId = warehouseId;
            OwnerId = ownerId;
            Name = name;
            AreaLevel1 = areaLevel1;
            AreaLevel2 = areaLevel2;
            AreaLevel3 = areaLevel3;
            AreaLevel4 = areaLevel4;
            SpaceLeft = spaceLeft;
            Opens = opens;
            Closed = closed;
            Latitude = latitude;
            Longitude = longitude;
            Freezers = freezer;
            Chillers = chiller;
        }

        [DataMember] public int WarehouseId { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public int SpaceLeft { get; set; }
        [DataMember] public double Latitude { get; set; }
        [DataMember] public double Longitude { get; set; }
        [DataMember] public string Opens { get; set; }
        [DataMember] public string Closed { get; set; }
        [DataMember] public string OwnerId { get; set; }
        [DataMember] public string AreaLevel1 { get; set; }
        [DataMember] public string AreaLevel2 { get; set; }
        [DataMember] public string AreaLevel3 { get; set; }
        [DataMember] public string AreaLevel4 { get; set; }
        [DataMember] public int Freezers { get; set; }
        [DataMember] public int Chillers { get; set; }
    }
}
