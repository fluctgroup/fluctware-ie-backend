using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.Commands
{
    public class InboundStockCommandHandler : IRequestHandler<InboundStockCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public InboundStockCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task<ValidationViewModel> Handle(InboundStockCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var warehouse = await _context.Warehouses
                        .Include(x=>x.Stocks)
                           .SingleOrDefaultAsync(x => x.Id == command.WarehouseId);
                    if (warehouse != null)
                    {
                        // verify if any stocks already exists
                        foreach (var item in command.Items)
                        {
                            if (item.Unique)
                            {
                                var stoq = await _context.Stocks.SingleOrDefaultAsync(x => x.SKU == item.SKU && x.Qty > 0);
                                if (stoq != null)
                                {
                                    return new ValidationViewModel()
                                    {
                                        Key = "server",
                                        Message = "Data is empty."
                                    };
                                }
                            }

                        }
                        warehouse.InboundStock(command.Items,
                            command.AuthorId, command.ProductId,command.Notes,command.From);
                        var result = await _warehouseRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                        if (result)
                        {
                            return null;
                        }
                    }
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class InboundStockIdentifiedCommandHandler : IdentifiedCommandHandler<InboundStockCommand, ValidationViewModel>
    {
        public InboundStockIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
