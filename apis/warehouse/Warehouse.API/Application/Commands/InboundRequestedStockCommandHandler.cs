using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.Commands
{
    public class InboundRequestedStockCommandHandler : IRequestHandler<InboundRequestedStockCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public InboundRequestedStockCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task<ValidationViewModel> Handle(InboundRequestedStockCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var warehouse = await _context.Warehouses
                                            .Include(x => x.Stocks)
                                            .Include(x => x.TransportRequests)
                                            .ThenInclude(x => x.Type)
                                            .Include(x => x.TransportRequests)
                                            .ThenInclude(x => x.Items)
                                            .Include(x => x.TransportRequests)
                                            .ThenInclude(x => x.Status)
                                                .SingleOrDefaultAsync(x => x.Id == command.WarehouseId); 
                    if (warehouse != null)
                    {
                        var req=warehouse.InboundStockShipped(command.RequestId);
                        if (int.TryParse(req.To, out var id)) 
                        {
                            var inboundWarehouse = await _context.Warehouses.Include(x => x.Stocks)
                                                   .SingleOrDefaultAsync(x => x.Id == id);
                            if (inboundWarehouse != null)
                            {
                                inboundWarehouse.InboundRequestedStock(req.Items, req.GetProductId(),
                               command.AuthorId, command.Notes);
                            }
                        }
                        
                        var result = await _warehouseRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                        if (result)
                        {
                            return null;
                        }
                    }
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class InboundRequestedStockIdentifiedCommandHandler : IdentifiedCommandHandler<InboundRequestedStockCommand, ValidationViewModel>
    {
        public InboundRequestedStockIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
