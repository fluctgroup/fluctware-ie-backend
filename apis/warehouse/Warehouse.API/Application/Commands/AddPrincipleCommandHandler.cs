using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.ProductAggregate;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.Commands
{
    public class AddPrincipleCommandHandler : IRequestHandler<AddPrincipleCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public AddPrincipleCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task<ValidationViewModel> Handle(AddPrincipleCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var principle = await _context.Principles
                           .SingleOrDefaultAsync(x => x.Name == command.Name);

                    if (principle != null)
                    {
                        return new ValidationViewModel()
                        {
                            Key = "server",
                            Message = "Principle name already taken."
                        };
                    }
                    principle = new Principle(command.Name);
                    _context.Principles.Add(principle);

                    await _warehouseRepository.UnitOfWork.SaveEntitiesAsync();
                    return null;
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class AddPrincipleIdentifiedCommandHandler : IdentifiedCommandHandler<AddPrincipleCommand, ValidationViewModel>
    {
        public AddPrincipleIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
