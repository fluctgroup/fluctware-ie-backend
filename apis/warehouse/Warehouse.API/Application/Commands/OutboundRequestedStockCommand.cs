using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
  public class OutboundRequestedStockCommand : IRequest<ValidationViewModel>
  {
    public OutboundRequestedStockCommand()
    {
    }
    public OutboundRequestedStockCommand(int requestId, int warehouseId ,string authorId,
        string notes, string type) : this()
    {
            Type = type;
            WarehouseId = warehouseId;
            RequestId = requestId;
            AuthorId = authorId;
            Notes = notes;
    }
        [DataMember] public int RequestId { get; set; }
        [DataMember] public int WarehouseId { get; set; }
        [DataMember] public string Type { get; set; }
        [DataMember] public string AuthorId { get; set; }
        [DataMember] public string Notes { get; set; }
    }
}
