using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.Infrastructure.Services;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.Commands
{
    public class EditStockCommandHandler : IRequestHandler<EditStockCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public EditStockCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task<ValidationViewModel> Handle(EditStockCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var stock = await _context.Stocks
                           .SingleOrDefaultAsync(x => x.Id == command.StockId);

                    if (stock == null)
                    {
                        return new ValidationViewModel()
                        {
                            Key = "server",
                            Message = "Stock not found."
                        };
                    }
                    stock.EditStock(command.Qty,stock.AuthorId,stock.ExpiryDate,stock.Quantifier,stock.Note,stock.SKU,stock.Unique);
                    await _warehouseRepository.UnitOfWork.SaveEntitiesAsync();
                    return null;
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class EditStockIdentifiedCommandHandler : IdentifiedCommandHandler<EditStockCommand, ValidationViewModel>
    {
        public EditStockIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
