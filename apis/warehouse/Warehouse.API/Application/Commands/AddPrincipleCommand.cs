using System;
using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
    public class AddPrincipleCommand : IRequest<ValidationViewModel>
    {
        public AddPrincipleCommand()
        {
        }
        public AddPrincipleCommand(string name) : this()
        {
            Name = name;
        }
        [DataMember] public string Name { get; set; }
    }
}
