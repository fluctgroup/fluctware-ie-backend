using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
  public class AddWarehouseCommand : IRequest<ValidationViewModel>
  {
    public AddWarehouseCommand()
    {
    }
    public AddWarehouseCommand(int spaceLeft, string opens, string closed,
        string name, string ownerId, string areaLevel1, string areaLevel2,
        string areaLevel3, string areaLevel4, double latitude, double longitude,
        int freezer, int chiller) : this()
    {
            SpaceLeft=spaceLeft;
            Opens=opens;
            Closed = closed;
            Name=name;
            OwnerId = ownerId;
            AreaLevel1 = areaLevel1;
            AreaLevel2 = areaLevel2;
            AreaLevel3 = areaLevel3;
            AreaLevel4 = areaLevel4;
            Latitude = latitude;
            Longitude = longitude;
            Freezer = freezer;
            Chiller = chiller;
        }
        [DataMember] public int SpaceLeft { get; set; }
        [DataMember] public string Opens { get; set; }
        [DataMember] public string Closed { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public string OwnerId { get; set; }
        [DataMember] public string AreaLevel1 { get; set; }
        [DataMember] public string AreaLevel2 { get; set; }
        [DataMember] public string AreaLevel3 { get; set; }
        [DataMember] public string AreaLevel4 { get; set; }
        [DataMember] public double Latitude { get; set; }
        [DataMember] public double Longitude { get; set; }
        [DataMember] public int Freezer { get; set; }
        [DataMember] public int Chiller { get; set; }
    }
}
