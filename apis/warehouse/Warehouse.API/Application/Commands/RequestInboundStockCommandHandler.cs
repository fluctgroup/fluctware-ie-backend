using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.Commands
{
    public class RequestInboundStockCommandHandler : IRequestHandler<RequestInboundStockCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public RequestInboundStockCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task<ValidationViewModel> Handle(RequestInboundStockCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var warehouse = await _context.Warehouses
                           .SingleOrDefaultAsync(x => x.Id == command.WarehouseId);
                    if (warehouse != null)
                    {
                        warehouse.RequestInboundStock(command.ProductId, command.To.ToString(),command.Items);
                        var result = await _warehouseRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                        if (result)
                        {
                            return null;
                        }
                    }
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class RequestInboundStockIdentifiedCommandHandler : IdentifiedCommandHandler<RequestInboundStockCommand, ValidationViewModel>
    {
        public RequestInboundStockIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
