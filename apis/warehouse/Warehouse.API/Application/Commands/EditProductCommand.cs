using System;
using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
  public class EditProductCommand : IRequest<ValidationViewModel>
  {
    public EditProductCommand()
    {
    }
    public EditProductCommand(int productId,string skuPrinciple, DateTime expireDate, bool hasExpiryDate,
            string name, bool hasRange, string authorId, string photoUrl, string rangeHigh,
            string rangeLow, int lowQty,int principleId, int expireDateLimit) : this()
    {
            ProductId = productId;
            SKUPrinciple = skuPrinciple;
            ExpireDate = expireDate;
            HasExpireDate = hasExpiryDate;
            Name = name;
            HasRange = hasRange;
            AuthorId = authorId;
            PhotoUrl = photoUrl;
            RangeHigh = rangeHigh;
            RangeLow = rangeLow;
            LowQty = lowQty;
            PrincipleId = principleId;
            ExpireDateLimit = expireDateLimit;
        }
        [DataMember] public int ProductId { get; set; }
        [DataMember] public int PrincipleId { get; set; }
        [DataMember] public string SKUPrinciple { get; set; }
        [DataMember] public DateTime ExpireDate { get; set; }
        [DataMember] public bool HasExpireDate { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public bool HasRange { get; set; }
        [DataMember] public string AuthorId { get; set; }
        [DataMember] public string PhotoUrl { get; set; }
        [DataMember] public string RangeHigh { get; set; }
        [DataMember] public string RangeLow { get; set; }
        [DataMember] public int LowQty { get; set; }
        [DataMember] public int ExpireDateLimit { get; set; }
    }
}
