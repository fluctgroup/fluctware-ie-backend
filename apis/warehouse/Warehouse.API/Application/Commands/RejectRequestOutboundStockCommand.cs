using System.Collections.Generic;
using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
  public class RejectRequestOutboundStockCommand : IRequest<ValidationViewModel>
  {
    public RejectRequestOutboundStockCommand()
    {
    }
    public RejectRequestOutboundStockCommand(int warehouseId, int requestId) : this()
    {
            WarehouseId = warehouseId;
            RequestId = requestId;
        }
        [DataMember] public int WarehouseId { get; set; }
        [DataMember] public int RequestId { get; set; }
    }
}
