using System.Collections.Generic;
using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
  public class RequestOutboundStockCommand : IRequest<ValidationViewModel>
  {
    public RequestOutboundStockCommand()
    {
    }
    public RequestOutboundStockCommand(int warehouseId, int productId, int to,
        List<TransportRequestItem> items) : this()
    {
            WarehouseId = warehouseId;
            ProductId = productId;
            To = to;
            Items = items;
        }
        [DataMember] public int WarehouseId { get; set; }
        [DataMember] public int ProductId { get; set; }
        [DataMember] public int To { get; set; }
        [DataMember] public List<TransportRequestItem> Items { get; set; }
    }
}
