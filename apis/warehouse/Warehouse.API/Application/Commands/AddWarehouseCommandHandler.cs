using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

using W = Warehouse.Domain.AggregatesModel.WarehouseAggregate.Warehouse;

namespace Warehouse.API.Application.Commands
{
    public class AddWarehouseCommandHandler : IRequestHandler<AddWarehouseCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public AddWarehouseCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task<ValidationViewModel> Handle(AddWarehouseCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var warehouse = await _context.Warehouses
                           .SingleOrDefaultAsync(x => x.Name == command.Name);

                    if (warehouse != null)
                    {
                        return new ValidationViewModel()
                        {
                            Key = "server",
                            Message = "Warehouse name already taken."
                        };
                    }
                    warehouse = new W(command.Name, command.SpaceLeft, command.Latitude,
                        command.Longitude, command.Opens, command.Closed, command.OwnerId,
                        command.AreaLevel1, command.AreaLevel2, command.AreaLevel3, command.AreaLevel4,
                        command.Freezer, command.Chiller);
                    _context.Warehouses.Add(warehouse);

                    await _warehouseRepository.UnitOfWork.SaveEntitiesAsync();
                    return null;
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class AddWarehouseIdentifiedCommandHandler : IdentifiedCommandHandler<AddWarehouseCommand, ValidationViewModel>
    {
        public AddWarehouseIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
