using System.Collections.Generic;
using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
  public class InboundStockCommand : IRequest<ValidationViewModel>
  {
    public InboundStockCommand()
    {
    }
    public InboundStockCommand(int warehouseId,int productId,string authorId,
        List<TransportRequestItem> items, string notes,string from) : this()
    {
            WarehouseId = warehouseId;
            ProductId = productId;
            AuthorId = authorId;
            Notes = notes;
            From = from;
    }
        [DataMember] public int WarehouseId { get; set; }
        [DataMember] public int ProductId { get; set; }
        [DataMember] public string AuthorId { get; set; }
        [DataMember] public string From { get; set; }
        [DataMember] public List<TransportRequestItem> Items { get; set; }
        [DataMember] public string Notes { get; set; }
    }
}
