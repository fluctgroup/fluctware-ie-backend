using System.Collections.Generic;
using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
    public class ReturnRequestCommand : IRequest<ValidationViewModel>
    {
        public ReturnRequestCommand()
        {
        }
        public ReturnRequestCommand(int warehouseId, int productId, string authorId,
        List<TransportRequestItem> items, string notes,string principle) : this()
        {
            WarehouseId = warehouseId;
            ProductId = productId;
            AuthorId = authorId;
            Notes = notes;
            Principle = principle;
            Items = items;
        }
        [DataMember] public int WarehouseId { get; set; }
        [DataMember] public int ProductId { get; set; }
        [DataMember] public string AuthorId { get; set; }
        [DataMember] public string Principle { get; set; }
        [DataMember] public List<TransportRequestItem> Items { get; set; }
        [DataMember] public string Notes { get; set; }
    }
}
