using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.API.Application.Commands;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace command.API.Application.Commands
{
    public class UpdateWarehouseCommandHandler : IRequestHandler<UpdateWarehouseCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public UpdateWarehouseCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task<ValidationViewModel> Handle(UpdateWarehouseCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var warehouse = await _context.Warehouses
                           .SingleOrDefaultAsync(x => x.Id == command.WarehouseId);
                    if (warehouse != null)
                    {
                        warehouse.UpdateWarehouse(command.OwnerId, command.Name, command.SpaceLeft,
                            command.Latitude, command.Longitude, command.Opens, command.Closed,
                            command.AreaLevel1, command.AreaLevel2, command.AreaLevel3, command.AreaLevel4,
                            command.Freezers, command.Chillers);
                        var result = await _warehouseRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                        if (result)
                        {
                            return null;
                        }
                    }
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class UpdateWarehouseIdentifiedCommandHandler : IdentifiedCommandHandler<UpdateWarehouseCommand, ValidationViewModel>
    {
        public UpdateWarehouseIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
