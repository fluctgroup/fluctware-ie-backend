using System.Runtime.Serialization;
using Library.Common.ViewModels;
using MediatR;

namespace Warehouse.API.Application.Commands
{
    [DataContract]
  public class InboundRequestedStockCommand : IRequest<ValidationViewModel>
  {
    public InboundRequestedStockCommand()
    {
    }
    public InboundRequestedStockCommand(int requestId, int warehouseId ,string authorId,
        string notes) : this()
    {
            WarehouseId = warehouseId;
            RequestId = requestId;
            AuthorId = authorId;
            Notes = notes;
    }
        [DataMember] public int RequestId { get; set; }
        [DataMember] public int WarehouseId { get; set; }
        [DataMember] public string AuthorId { get; set; }
        [DataMember] public string Notes { get; set; }
    }
}
