using System;
using System.Threading;
using System.Threading.Tasks;
using Library.Common.Commands;
using Library.Common.Infrastructure.Idempotency;
using Library.Common.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.Commands
{
    public class ApproveRequestInboundStockCommandHandler : IRequestHandler<ApproveRequestInboundStockCommand, ValidationViewModel>
    {
        private readonly IWarehouseRepository _warehouseRepository;
        private readonly WarehouseContext _context;

        public ApproveRequestInboundStockCommandHandler(WarehouseContext context,
          IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _context = context;
        }

        public async Task<ValidationViewModel> Handle(ApproveRequestInboundStockCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (command != null)
                {
                    var warehouse = await _context.Warehouses
                        .Include(x => x.TransportRequests)
                        .ThenInclude(x => x.Type)
                        .Include(x => x.TransportRequests)
                        .ThenInclude(x => x.Status)
                           .SingleOrDefaultAsync(x => x.Id == command.WarehouseId);
                    if (warehouse != null)
                    {
                        warehouse.ApproveRequestInboundStock(command.RequestId);
                        var result = await _warehouseRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                        if (result)
                        {
                            return null;
                        }
                    }
                }

                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = "Data is empty."
                };
            }
            catch (Exception ex)
            {
                return new ValidationViewModel()
                {
                    Key = "server",
                    Message = ex.Message
                };
            }
        }
    }

    // Use for Idempotency in Command process
    public class ApproveRequestInboundStockIdentifiedCommandHandler : IdentifiedCommandHandler<ApproveRequestInboundStockCommand, ValidationViewModel>
    {
        public ApproveRequestInboundStockIdentifiedCommandHandler(IMediator mediator, IRequestManager requestManager) : base(mediator,
          requestManager)
        {
        }

        protected override ValidationViewModel CreateResultForDuplicateRequest()
        {
            return null; // Ignore duplicate requests for processing order.
        }
    }
}
