using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.API.ViewModel;
using Warehouse.Domain.AggregatesModel.ProductAggregate;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using W = Warehouse.Domain.AggregatesModel.WarehouseAggregate.Warehouse;
using Warehouse.Infrastructure;
using Warehouse.Domain.Exceptions;
using System.Linq.Expressions;

namespace Warehouse.API.Application.Queries
{
    public class WarehouseQueries : IWarehouseQueries
    {
        private readonly WarehouseContext _context;
        private readonly IWarehouseRepository _repository;
        public WarehouseQueries(WarehouseContext context, IWarehouseRepository repository)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<IEnumerable<TransportViewModel>> FetchHistoryTransportByWarehouse(int warehouseId, CancellationToken cancellationToken = default)
        {
            var warehouse = await _context.Warehouses.Include(x => x.TransportRequests)
                .ThenInclude(x => x.Items)
                .Include(x => x.TransportRequests)
                .ThenInclude(x => x.Status)
                .Include(x => x.TransportRequests)
                .ThenInclude(x => x.Type)
                .SingleOrDefaultAsync(x => x.Id == warehouseId);
            if (warehouse != null)
            {
                var principles = await _context.Principles.ToListAsync();
                var w = warehouse.TransportRequests.Where(x => x.GetStatusId() == TransportRequestStatus.Rejected.Id || x.GetStatusId() == TransportRequestStatus.Delivered.Id || x.GetStatusId() == TransportRequestStatus.Cancelled.Id).ToList();
                var requests = w.Select(x =>
                    new TransportViewModel(x, GetNameUser(principles, x.From), GetNameUser(principles, x.To)))
                    .ToList();
                return requests;
            }
            return null;
        }

        public async Task<IEnumerable<TransportViewModel>> FetchOngoingTransportByWarehouse(int warehouseId, CancellationToken cancellationToken = default)
        {
            var principles = await _context.Principles.ToListAsync();
            var requests = await _context.TransportRequests
                .Include(x => x.Status)
                .Include(x => x.Type)
                .Include(x => x.Items)
                .Where(x => x.Status.Id != TransportRequestStatus.Delivered.Id &&
                x.Status.Id != TransportRequestStatus.Rejected.Id &&
                x.Status.Id != TransportRequestStatus.Cancelled.Id &&
                (x.From == warehouseId.ToString() || x.To == warehouseId.ToString()))
                .ToListAsync();
            
            return requests.Select(x => new TransportViewModel(x, GetNameUser(principles, x.From), GetNameUser(principles, x.To))).ToList();
        }

        public async Task<IEnumerable<InventoryViewModel>> InventoriesByWarehouseId(int warehouseId, CancellationToken cancellationToken = default)
        {
            var stocks= await _context.Stocks.Include(x => x.Product)
                .Where(x => x.WarehouseId == warehouseId && x.Qty > 0)
                .ToListAsync(cancellationToken);
            return stocks.Select(x => new InventoryViewModel(x)).ToList();
                
        }

        public async Task<InventoryViewModel> InventoryById(int stockId, CancellationToken cancellationToken = default)
        {
            var stock = await _context.Stocks.Include(x => x.Product)
                .SingleOrDefaultAsync(x => x.Id == stockId, cancellationToken);
            return new InventoryViewModel(stock);
        }
        public async Task<TransportViewModel> TransportDetailById(int transportId, CancellationToken cancellationToken = default)
        {
            var transport = await _context.TransportRequests
                .Include(x => x.Status)
                .Include(x => x.Type)
                .Include(x => x.Items)
                .SingleOrDefaultAsync(x => x.Id == transportId, cancellationToken);
            if (transport != null)
            {
                var principles = await _context.Principles.ToListAsync();
                return new TransportViewModel(transport, GetNameUser(principles, transport.From), GetNameUser(principles, transport.To));
            }
            return null;
        }
        public string GetNameUser(List<Principle> principles, string name)
        {
            if (name.Contains("principle-"))
            {
                var id = name.Split("principle-").Last();
                if (int.TryParse(id, out var ids))
                {
                    var principle = principles.SingleOrDefault(x => x.Id == ids);
                    if (principle != null)
                    {
                        return principle.Name;
                    }
                }
                return null;
            }
            else if (int.TryParse(name, out var ids))
            {
                var warehouse = _context.Warehouses.SingleOrDefault(x => x.Id == ids);
                if (warehouse != null)
                {
                    return warehouse.Name;
                }
                return null;
            }
            return name;
        }

        public async Task<IEnumerable<StockExpiryPairViewModel>> CheckAllWarehouseStock(int warehouseId, string sku, CancellationToken cancellationToken = default)
        {
            var warehouse = await _context.Warehouses
                    .Include(x => x.Stocks)
                    .ThenInclude(x => x.Product)
                    .FirstOrDefaultAsync(x => x.Id == warehouseId);

            IEnumerable<StockExpiryPairViewModel> result = null;
            if (warehouse != null)
            {
                var stocks = warehouse.GetAllStock(sku);
                if (stocks != null && stocks.Count()>0)
                {
                    result = stocks.Select(x => new StockExpiryPairViewModel()
                    {
                        Amount = x.Qty,
                        ExpiryDate = x.ExpiryDate,
                        StockId = x.Id
                    }).ToList();
                    return result;
                }
                throw new WarehouseDomainException("Stock tidak ada.");
            }
            throw new WarehouseDomainException("Warehouse Tidak ditemukan.");
        }
        public async Task<IEnumerable<WarehouseStockPairViewModel>> CheckStockByExpireDate(int inquirer, string sku, DateTime startDate, DateTime endDate, bool any, CancellationToken cancellationToken = default)
        {
            var result = new List<WarehouseStockPairViewModel>();
            Expression<Func<Stock, bool>> expression = x => x.SKU == sku &&
                             x.Qty > 0 &&
                             x.WarehouseId != inquirer &&
                             x.ExpiryDate >= startDate &&
                             x.ExpiryDate <= endDate;
            if (any)
            {
                expression = x => x.WarehouseId != inquirer && x.SKU == sku && x.Qty > 0;
            }
            var stock = await _context.Stocks
                    .Include(x => x.Product)
                    .Where(expression)
                    .Select(x => new
                    {
                        warehouseId = x.WarehouseId,
                        qty = x.Qty,
                        expireDate = x.ExpiryDate,
                        stockId = x.Id
                    })
                    .ToListAsync();

            if (stock != null && stock.Count() > 0)
            {
                var grpStock = stock.GroupBy(x => x.warehouseId).ToList();
                foreach (var grp in grpStock)
                {
                    var warehouse = await _context.Warehouses.SingleOrDefaultAsync(x => x.Id == grp.Key);
                    if (warehouse == null)
                    {
                        throw new WarehouseDomainException("Warehouse Tidak ditemukan.");
                    }

                    result.Add(new WarehouseStockPairViewModel()
                    {
                        WarehouseId = warehouse.Id,
                        WarehouseName = warehouse.Name,
                        Stocks = grp.Select(x => new StockExpiryPairViewModel()
                        {
                            Amount = x.qty,
                            ExpiryDate = x.expireDate,
                            StockId = x.stockId
                        }).ToList()
                    });
                }
                return result;
            }
            throw new WarehouseDomainException("Stock tidak ada.");
        }

        public async Task<List<InventoryHistoryViewModel>> InventoryHistoryById(int stockId, CancellationToken cancellationToken = default)
        {
            var stock = await _context.Stocks
                       .Include(x => x.Product)
                       .Include(x => x.StockAdjustmentLogs)
                       .SingleOrDefaultAsync(x => x.Id == stockId,cancellationToken);

            if (stock != null)
            {
                var logs=stock.StockAdjustmentLogs.Select(x => new InventoryHistoryViewModel()
                {
                    Note = x.Note,
                    ProductName = stock.Product.Name,
                    StockId = stock.Id,
                    BeforeStock = x.BeforeStock,
                    AfterStock = x.AfterStock,
                    ModifiedBy = x.ModifiedBy,
                    ModifiedDate = x.ModifiedDate
                }).ToList();

                return logs;
            }
            throw new WarehouseDomainException("Stock tidak ada.");
        }
    }
}
