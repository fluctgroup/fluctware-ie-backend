

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.API.ViewModel;

namespace Warehouse.API.Application.Queries
{
    public interface IWarehouseQueries
    {
        Task<IEnumerable<InventoryViewModel>> InventoriesByWarehouseId(int warehouseId, CancellationToken cancellationToken = default(CancellationToken));
        Task<InventoryViewModel> InventoryById(int stockId,CancellationToken cancellationToken = default(CancellationToken));
        Task<List<InventoryHistoryViewModel>> InventoryHistoryById(int stockId, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<TransportViewModel>> FetchOngoingTransportByWarehouse(int warehouseId, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<TransportViewModel>> FetchHistoryTransportByWarehouse(int warehouseId, CancellationToken cancellationToken = default(CancellationToken));
        Task<TransportViewModel> TransportDetailById(int transportId, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<WarehouseStockPairViewModel>> CheckStockByExpireDate(int inquirer,string sku, DateTime startDate, DateTime endDate, bool any, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<StockExpiryPairViewModel>> CheckAllWarehouseStock(int warehouseId, string sku, CancellationToken cancellationToken = default);
    }
}
