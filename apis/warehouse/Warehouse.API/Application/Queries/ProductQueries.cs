using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.API.ViewModel;
using Warehouse.Domain.AggregatesModel.ProductAggregate;
using Warehouse.Infrastructure;

namespace Warehouse.API.Application.Queries
{
    public class ProductQueries : IProductQueries
    {
        private readonly WarehouseContext _context;
        private readonly IProductRepository _repository;
        public ProductQueries(WarehouseContext context, IProductRepository repository)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<IEnumerable<PrincipleViewModel>> GetAllPrinciple(CancellationToken token = default)
        {
            return await _context.Principles
                .Select(x => new PrincipleViewModel()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToListAsync(token);
        }

        public async Task<IEnumerable<ProductViewModel>> GetAllProducts(CancellationToken token = default(CancellationToken))
        {
            return await _context.Products
                .Select(x => new ProductViewModel(x)).ToListAsync(token);
        }

        public async Task<PrincipleViewModel> GetPrincipleById(int id, CancellationToken token = default)
        {
            var principle = await _context.Principles.SingleOrDefaultAsync(x => x.Id == id);
            return new PrincipleViewModel(principle);
        }

        public async Task<IEnumerable<PrincipleInventoryViewModel>> GetPrincipleInventoryById(int id, CancellationToken token = default)
        {
            var principle = await _context.Principles.FirstOrDefaultAsync(x => x.Id == id);
            var products = await _context.Products.Include(x=>x.Principle)
                                .Where(x => x.Principle.Id == principle.Id)
                                .Select(x=>x.SKUPrinciple).Distinct().ToListAsync();
            var xs = _context.Stocks.ToList();
            var stock = await _context.Stocks.Include(x=>x.Product).Where(x => products.Contains(x.SKU)).ToListAsync();
            var warehouseId=stock.Select(x => x.WarehouseId).Distinct();
            var warehouse = _context.Warehouses.Where(x => warehouseId.Contains(x.Id));
            var responses = new List<PrincipleInventoryViewModel>();

            foreach(var s in stock)
            {
                var w = await warehouse.FirstOrDefaultAsync(x => x.Id == s.WarehouseId);
                var res = new PrincipleInventoryViewModel(principle,s, w);
                responses.Add(res);
            }
            return responses;
        }

        public async Task<ProductViewModel> GetProductById(int id, CancellationToken token = default)
        {
            var product = await _repository.SingleOrDefaultAsync(x => x.Id == id);
            return new ProductViewModel(product);
        }
        public async Task<ProductViewModel> GetProductBySKU(string sku, CancellationToken token = default)
        {
            var product = await _repository.SingleOrDefaultAsync(x => x.SKUPrinciple == sku);
            if (product != null)
            {
                return new ProductViewModel(product);
            }
            return null;
        }
    }
}
