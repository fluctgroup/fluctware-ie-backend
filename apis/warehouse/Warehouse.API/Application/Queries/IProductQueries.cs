

using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Warehouse.API.ViewModel;

namespace Warehouse.API.Application.Queries
{
    public interface IProductQueries
    {
        Task<IEnumerable<PrincipleViewModel>> GetAllPrinciple(CancellationToken token = default(CancellationToken));
        Task<PrincipleViewModel> GetPrincipleById(int id,CancellationToken token = default(CancellationToken));
        Task<IEnumerable<PrincipleInventoryViewModel>> GetPrincipleInventoryById(int id, CancellationToken token = default(CancellationToken));
        Task<IEnumerable<ProductViewModel>> GetAllProducts(CancellationToken token=default(CancellationToken));
        Task<ProductViewModel> GetProductById(int id,CancellationToken token = default(CancellationToken));
        Task<ProductViewModel> GetProductBySKU(string sku, CancellationToken token = default(CancellationToken));
    }
}
