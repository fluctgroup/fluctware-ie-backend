using Library.Common;

namespace Warehouse.API
{
    public class WarehouseSettings : FileSettings
    {
        public string BaseUrl { get; set; }

        public string EventBusConnection { get; set; }

        public bool UseCustomizationData { get; set; }
    }
}