﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Warehouse.API.Migrations
{
    public partial class expiredLimit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExpireDateLimit",
                table: "Products",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpireDateLimit",
                table: "Products");
        }
    }
}
