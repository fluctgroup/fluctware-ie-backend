﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Warehouse.API.Migrations.IntegrationEventLog
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "IntegrationEventLog",
                table => new
                {
                    EventId = table.Column<Guid>(),
                    EventTypeName = table.Column<string>(),
                    State = table.Column<int>(),
                    TimesSent = table.Column<int>(),
                    CreationTime = table.Column<DateTime>(),
                    Content = table.Column<string>()
                },
                constraints: table => { table.PrimaryKey("PK_IntegrationEventLog", x => x.EventId); });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "IntegrationEventLog");
        }
    }
}