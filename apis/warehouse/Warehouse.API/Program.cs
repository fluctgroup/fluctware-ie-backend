using System;
using System.IO;
using Autofac.Extensions.DependencyInjection;
using Warehouse.API.Infrastructure;
using Library.Common.Infrastructure.Services;
using Library.IntegrationEventLog;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog;
using WebHost.Customization;
using ILogger = Serilog.ILogger;
using Warehouse.Infrastructure;

namespace Warehouse.API
{
    public class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;

        public static readonly string AppName =
            Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static int Main(string[] args)
        {
            var configuration = GetConfiguration();

            Log.Logger = CreateSerilogLogger(configuration);

            try
            {
                Log.Information("Configuring web host ({ApplicationContext})...", AppName);
                var host = CreateWebHostBuilder(args).Build()
                    .MigrateDbContext<WarehouseContext>((context, services) =>
                    {
                        var setting = services.GetRequiredService<IOptions<WarehouseSettings>>();
                        var webHostEnv = services.GetRequiredService<IWebHostEnvironment>();
                        var logger = services.GetRequiredService<ILogger<WarehouseContextSeed>>();
                        var fileService = services.GetRequiredService<IFileService>();

                        new WarehouseContextSeed()
                            .SeedAsync(context, fileService, setting, webHostEnv, logger)
                            .Wait();
                    })
                    .MigrateDbContext<IntegrationEventLogContext>((_, __) => { });
                Log.Information("Starting web host ({ApplicationContext})...", AppName);
                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            var seqServerUrl = configuration["Serilog:SeqServerUrl"];
            var logstashUrl = configuration["Serilog:LogstashgUrl"];
            return new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.Seq(string.IsNullOrWhiteSpace(seqServerUrl) ? "http://seq" : seqServerUrl)
                .WriteTo.Http(string.IsNullOrWhiteSpace(logstashUrl) ? "http://logstash:8080" : logstashUrl)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        private static IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .AddEnvironmentVariables();

            var config = builder.Build();

            if (config.GetValue("UseVault", false))
                builder.AddAzureKeyVault(
                    $"https://{config["Vault:Name"]}.vault.azure.net/",
                    config["Vault:ClientId"],
                    config["Vault:ClientSecret"]);

            return builder.Build();
        }

        public static IHostBuilder CreateWebHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.CaptureStartupErrors(false)
                        .UseStartup<Startup>()
                        .ConfigureAppConfiguration((builderContext, config) => { config.AddEnvironmentVariables(); })
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .UseSerilog();
                });
        }
    }
}