using System;
using Library.Abstraction;
using Warehouse.Domain.AggregatesModel.ProductAggregate;

namespace Warehouse.Infrastructure.Repositories
{
  public class ProductRepository
    : Repository<Product, WarehouseContext>, IProductRepository
    {
    private readonly WarehouseContext _context;

    public ProductRepository(WarehouseContext context) : base(context)
    {
      _context = context ?? throw new ArgumentNullException(nameof(context));
    }
  }
}
