using System;
using Library.Abstraction;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using W=Warehouse.Domain.AggregatesModel.WarehouseAggregate.Warehouse;

namespace Warehouse.Infrastructure.Repositories
{
  public class WarehouseRepository
    : Repository<W, WarehouseContext>, IWarehouseRepository
  {
    private readonly WarehouseContext _context;

    public WarehouseRepository(WarehouseContext context) : base(context)
    {
      _context = context ?? throw new ArgumentNullException(nameof(context));
    }
  }
}
