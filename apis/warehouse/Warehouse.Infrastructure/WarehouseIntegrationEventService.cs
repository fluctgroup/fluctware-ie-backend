using System;
using System.Data.Common;
using System.Threading.Tasks;
using Library.EventBus.Abstractions;
using Library.EventBus.Events;
using Library.IntegrationEventLog.Services;
using Library.IntegrationEventLog.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Warehouse.Infrastructure
{
  public class WarehouseIntegrationEventService : IWarehouseIntegrationEventService
  {
    private readonly WarehouseContext _context;
    private readonly IEventBus _eventBus;
    private readonly IIntegrationEventLogService _eventLogService;
    private readonly Func<DbConnection, IIntegrationEventLogService> _integrationEventLogServiceFactory;

    public WarehouseIntegrationEventService(IEventBus eventBus, WarehouseContext context,
      Func<DbConnection, IIntegrationEventLogService> integrationEventLogServiceFactory)
    {
      _context = context ?? throw new ArgumentNullException(nameof(context));
      _integrationEventLogServiceFactory = integrationEventLogServiceFactory ??
                                           throw new ArgumentNullException(
                                             nameof(integrationEventLogServiceFactory));
      _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
      _eventLogService = _integrationEventLogServiceFactory(_context.Database.GetDbConnection());
    }

    public async Task PublishThroughEventBusAsync(IntegrationEvent evt)
    {
      await SaveEventAndWarehouseContextChangesAsync(evt);
      _eventBus.Publish(evt);
      await _eventLogService.MarkEventAsPublishedAsync(evt);
    }

    public async Task SaveEventAndWarehouseContextChangesAsync(IntegrationEvent evt)
    {
      //Use of an EF Core resiliency strategy when using multiple DbContexts within an explicit BeginTransaction():
      //See: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency            
      await ResilientTransaction.New(_context)
        .ExecuteAsync(async () =>
        {
          // Achieving atomicity between original catalog database operation and the IntegrationEventLog thanks to a local transaction
          await _context.SaveChangesAsync();
          await _eventLogService.SaveEventAsync(evt,
            _context.Database.CurrentTransaction.GetDbTransaction());
        });
    }
  }
}
