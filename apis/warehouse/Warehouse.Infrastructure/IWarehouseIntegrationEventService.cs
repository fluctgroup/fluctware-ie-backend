using System.Threading.Tasks;
using Library.EventBus.Events;

namespace Warehouse.Infrastructure
{
  public interface IWarehouseIntegrationEventService
  {
    Task SaveEventAndWarehouseContextChangesAsync(IntegrationEvent evt);
    Task PublishThroughEventBusAsync(IntegrationEvent evt);
  }
}
