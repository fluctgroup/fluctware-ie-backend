using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Library.Abstraction;
using Library.Common.Extensions;
using Library.Common.Infrastructure.EntityConfigurations;
using Library.Common.Infrastructure.Idempotency;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain.AggregatesModel.ProductAggregate;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;
using Warehouse.Infrastructure.EntityConfigurations;
using W = Warehouse.Domain.AggregatesModel.WarehouseAggregate.Warehouse;

namespace Warehouse.Infrastructure
{
    public class WarehouseContext : DbContext, IUnitOfWork
    {
        private readonly IMediator _mediator;

        public WarehouseContext(DbContextOptions<WarehouseContext> options) : base(options)
        {
        }

        public WarehouseContext(DbContextOptions<WarehouseContext> options, IMediator mediator) :
            base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));

            Debug.WriteLine("WarehouseContext::ctor ->" + GetHashCode());
        }

        public DbSet<ClientRequest> ClientRequests { get; set; }
        public DbSet<W> Warehouses { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Principle> Principles { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<StockAdjustmentLog> StockAdjustmentLogs { get; set; }
        public DbSet<TransportRequest> TransportRequests { get; set; }
        public DbSet<TransportRequestItem> TransportRequestItems { get; set; }
        public DbSet<TransportRequestStatus> TransportRequestStatus { get; set; }
        public DbSet<TransportType> TransportTypes { get; set; }
        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            // Dispatch Domain Events collection. 
            // Choices:
            // A) Right BEFORE committing data (EF SaveChanges) into the DB will make a single transaction including  
            // side effects from the domain event handlers which are using the same DbContext with "InstancePerLifetimeScope" or "scoped" lifetime
            // B) Right AFTER committing data (EF SaveChanges) into the DB will make multiple transactions. 
            // You will need to handle eventual consistency and compensatory actions in case of failures in any of the Handlers. 
            await _mediator.DispatchDomainEventsAsync(this);

            // After executing this line all the changes (from the Command Handler and Domain Event Handlers) 
            // performed throught the DbContext will be commited
            var result = await SaveChangesAsync(cancellationToken);

            return true;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ClientRequestEntityTypeConfiguration());
            builder.ApplyConfiguration(new WarehouseEntityTypeConfiguration());
            builder.ApplyConfiguration(new OwnerEntityTypeConfiguration());
            builder.ApplyConfiguration(new PrincipleEntityTypeConfiguration());
            builder.ApplyConfiguration(new ProductEntityTypeConfiguration());
            builder.ApplyConfiguration(new StockAdjustmentLogEntityTypeConfiguration());
            builder.ApplyConfiguration(new StockEntityTypeConfiguration());
            builder.ApplyConfiguration(new TransportRequestEntityTypeConfiguration());
            builder.ApplyConfiguration(new TransportRequestItemEntityTypeConfiguration());
            builder.ApplyConfiguration(new TransportRequestStatusEntityTypeConfiguration());
            builder.ApplyConfiguration(new TransportTypeEntityTypeConfiguration());
        }
    }

    public class NoMediator : IMediator
    {
        public Task Publish(object notification, CancellationToken cancellationToken = new CancellationToken())
        {
            return Task.CompletedTask;
        }

        public Task Publish<TNotification>(TNotification notification,
            CancellationToken cancellationToken = default) where TNotification : INotification
        {
            return Task.CompletedTask;
        }

        public Task<TResponse> Send<TResponse>(IRequest<TResponse> request,
            CancellationToken cancellationToken = default)
        {
            return Task.FromResult(default(TResponse));
        }

        public Task<object> Send(object request, CancellationToken cancellationToken = default)
        {
            return Task.FromResult(default(object));
        }

        public Task Send(IRequest request, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }
    }
}