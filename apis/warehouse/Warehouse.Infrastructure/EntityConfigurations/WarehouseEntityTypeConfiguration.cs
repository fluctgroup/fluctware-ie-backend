using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using W = Warehouse.Domain.AggregatesModel.WarehouseAggregate.Warehouse;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class WarehouseEntityTypeConfiguration
        : IEntityTypeConfiguration<W>
    {
        public void Configure(EntityTypeBuilder<W> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(ci => ci.Name)
                .IsRequired();

            builder.Property(ci => ci.SpaceLeft);

            builder.Property(ci => ci.Opens);

            builder.Property(ci => ci.Closed);

            builder.Property(ci => ci.Latitude);

            builder.Property(ci => ci.Longitude);

            builder.Property(ci => ci.AreaLevel1);

            builder.Property(ci => ci.AreaLevel2);

            builder.Property(ci => ci.AreaLevel3);

            builder.Property(ci => ci.AreaLevel4);

            builder.Property(ci => ci.Freezers);

            builder.Property(ci => ci.Chillers);

            builder.Property(ci => ci.UpdatedDate)
                .IsRequired();

            builder
              .Property<string>("_ownerId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasColumnName("_ownerId")
              .IsRequired();

            var navigation = builder.Metadata.FindNavigation(nameof(W.Owner));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.HasOne(o => o.Owner)
                .WithMany()
              .HasForeignKey("OwnerId");
        }
    }
}