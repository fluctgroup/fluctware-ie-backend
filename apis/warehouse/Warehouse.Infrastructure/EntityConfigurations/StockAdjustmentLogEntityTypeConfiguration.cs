using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class StockAdjustmentLogEntityTypeConfiguration
        : IEntityTypeConfiguration<StockAdjustmentLog>
    {
        public void Configure(EntityTypeBuilder<StockAdjustmentLog> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(ci => ci.Note);

            builder.Property(ci => ci.StockId)
                .IsRequired();

            builder.Property(ci => ci.BeforeStock)
                .IsRequired();

            builder.Property(ci => ci.AfterStock)
                .IsRequired();

            builder.Property(ci => ci.ModifiedDate)
                .IsRequired();

            builder.Property(ci => ci.ModifiedBy)
                .IsRequired();

        }
    }
}