using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class TransportRequestEntityTypeConfiguration
        : IEntityTypeConfiguration<TransportRequest>
    {
        public void Configure(EntityTypeBuilder<TransportRequest> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(ci => ci.From)
                .IsRequired();

            builder.Property(ci => ci.To)
                .IsRequired();

            builder.Property(ci => ci.InboundTime)
                .IsRequired();

            builder
              .Property<int>("_statusId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasColumnName("StatusId")
              .IsRequired();

            builder
              .Property<int>("_typeId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasColumnName("TypeId")
              .IsRequired();

            builder
              .Property<int>("_productId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasColumnName("ProductId")
              .IsRequired();

            var navigation = builder.Metadata.FindNavigation(nameof(TransportRequest.Product));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.HasOne(o => o.Product)
              .WithMany()
              .HasForeignKey("_productId");
            
            builder.HasOne(o => o.Type)
              .WithMany()
              .HasForeignKey("_typeId");
            
            builder.HasOne(o => o.Status)
              .WithMany()
              .HasForeignKey("_statusId");

            builder.HasMany(ci => ci.Items)
              .WithOne()
              .HasForeignKey(x => x.RequestId);
        }
    }
}