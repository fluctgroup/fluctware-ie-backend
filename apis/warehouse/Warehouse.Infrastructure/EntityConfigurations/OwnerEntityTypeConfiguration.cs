using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class OwnerEntityTypeConfiguration
        : IEntityTypeConfiguration<Owner>
    {
        public void Configure(EntityTypeBuilder<Owner> builder)
        {
            builder.HasKey(ci => ci.OwnerId);

            builder.Property(ci => ci.OwnerName)
                .IsRequired();

            builder.Property(ci => ci.OwnerEmail)
                .IsRequired();

            builder.Property(ci => ci.OwnerPhone)
                .IsRequired();

        }
    }
}