using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.AggregatesModel.ProductAggregate;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class PrincipleEntityTypeConfiguration
        : IEntityTypeConfiguration<Principle>
    {
        public void Configure(EntityTypeBuilder<Principle> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(ci => ci.Name)
                .IsRequired();
            
        }
    }
}