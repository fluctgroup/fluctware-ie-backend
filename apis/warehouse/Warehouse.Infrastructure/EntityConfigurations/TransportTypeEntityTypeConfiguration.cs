using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class TransportTypeEntityTypeConfiguration
        : IEntityTypeConfiguration<TransportType>
    {
        public void Configure(EntityTypeBuilder<TransportType> builder)
        {
            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id)
              .HasDefaultValue(1)
              .ValueGeneratedNever()
              .IsRequired();

            builder.Property(o => o.Name)
              .HasMaxLength(200)
              .IsRequired();
        }
    }
}