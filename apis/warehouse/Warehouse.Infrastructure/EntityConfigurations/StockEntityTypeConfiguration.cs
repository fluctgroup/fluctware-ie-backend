using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class StockEntityTypeConfiguration
        : IEntityTypeConfiguration<Stock>
    {
        public void Configure(EntityTypeBuilder<Stock> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(ci => ci.WarehouseId)
                .IsRequired();

            builder
              .Property<int>("_productId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasColumnName("ProductId")
              .IsRequired();

            builder.Property(ci => ci.ExpiryDate);

            builder.Property(ci => ci.InboundDate)
                .IsRequired();

            builder.Property(ci => ci.Qty)
                .IsRequired();

            builder.Property(ci => ci.Note);

            builder.Property(ci => ci.SKU)
                .IsRequired();

            builder.Property(ci => ci.AuthorId)
                .IsRequired();

            builder.Property(ci => ci.Quantifier)
                .IsRequired();

            builder.Property(ci => ci.Unique)
                .IsRequired();

            var navigation = builder.Metadata.FindNavigation(nameof(Stock.Product));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.HasOne(o => o.Product)
              .WithMany()
              .HasForeignKey("_productId");

            builder.HasMany(ci => ci.StockAdjustmentLogs)
              .WithOne()
              .HasForeignKey(x => x.StockId);
        }
    }
}