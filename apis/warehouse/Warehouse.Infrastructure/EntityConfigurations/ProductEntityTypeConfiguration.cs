using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.AggregatesModel.ProductAggregate;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class ProductEntityTypeConfiguration
        : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(ci => ci.SKUPrinciple)
                .IsRequired();

            builder.Property(ci => ci.ExpireDate);

            builder.Property(ci => ci.HasExpireDate)
                .IsRequired();

            builder.Property(ci => ci.Name)
                .IsRequired();

            builder.Property(ci => ci.HasRange)
                .IsRequired();

            builder.Property(ci => ci.AuthorId)
                .IsRequired();

            builder.Property(ci => ci.PhotoUrl);

            builder.Property(ci => ci.RangeHigh);

            builder.Property(ci => ci.RangeLow);

            builder.Property(ci => ci.LowQty)
                .IsRequired();

            builder.Property(ci => ci.ExpireDateLimit)
                .IsRequired();
            builder
              .Property<int>("_principleId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasColumnName("PrincipleId")
              .IsRequired();

            var navigation = builder.Metadata.FindNavigation(nameof(Product.Principle));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.HasOne(o => o.Principle)
              .WithMany()
              .HasForeignKey("_principleId");

        }
    }
}