using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehouse.Domain.AggregatesModel.WarehouseAggregate;

namespace Warehouse.Infrastructure.EntityConfigurations
{
    internal class TransportRequestItemEntityTypeConfiguration
        : IEntityTypeConfiguration<TransportRequestItem>
    {
        public void Configure(EntityTypeBuilder<TransportRequestItem> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(ci => ci.RequestId)
                .IsRequired();

            builder.Property(ci => ci.SKU)
                .IsRequired();

            builder.Property(ci => ci.ExpiryDate)
                .IsRequired();

            builder.Property(ci => ci.Unique)
                .IsRequired();

            builder.Property(ci => ci.Qty)
                .IsRequired();

            builder.Property(ci => ci.Quantifier)
                .IsRequired();
        }
    }
}