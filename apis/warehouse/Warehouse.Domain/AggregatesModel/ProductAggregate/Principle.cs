﻿using Library.Abstraction;
using System;
using System.Collections.Generic;

namespace Warehouse.Domain.AggregatesModel.ProductAggregate
{
    public class Principle: Entity
    {
        public string Name { get; private set; }
        public Principle(string name)
        {
            Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
        }
        public void Update(string name)
        {
            Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
        }
    }
}
