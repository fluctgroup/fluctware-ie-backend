using Library.Abstraction;

namespace Warehouse.Domain.AggregatesModel.ProductAggregate
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
