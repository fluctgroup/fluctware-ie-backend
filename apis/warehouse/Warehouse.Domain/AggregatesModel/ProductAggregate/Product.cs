﻿using Library.Abstraction;
using System;
using Warehouse.Domain.Exceptions;

namespace Warehouse.Domain.AggregatesModel.ProductAggregate
{
    public class Product : Entity, IAggregateRoot
    {
        private int _principleId;

        protected Product()
        {

        }
        public Product(string skuPrinciple, DateTime expireDate, bool hasExpiryDate,
            string name, bool hasRange, string authorId, string photoUrl, string rangeHigh,
            string rangeLow, int lowQty,int principleId, int expireDateLimit) :base()
        {
            Edit(skuPrinciple, expireDate, hasExpiryDate, name, hasRange, authorId, photoUrl, 
                rangeHigh,rangeLow, lowQty, principleId,expireDateLimit);
        }
        public void Edit(string skuPrinciple, DateTime expireDate, bool hasExpiryDate,
            string name, bool hasRange, string authorId, string photoUrl, string rangeHigh,
            string rangeLow, int lowQty, int principleId,int expireDateLimit)
        {
            _principleId = principleId > 0 ? principleId : throw new ArgumentNullException(nameof(principleId));
            SKUPrinciple = !string.IsNullOrWhiteSpace(skuPrinciple) ? skuPrinciple : throw new ArgumentNullException(nameof(skuPrinciple));
            ExpireDate = expireDate;
            HasExpireDate = hasExpiryDate;
            ExpireDateLimit = expireDateLimit;
            Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
            HasRange = hasRange;
            AuthorId = !string.IsNullOrWhiteSpace(authorId) ? authorId : throw new ArgumentNullException(nameof(authorId));
            PhotoUrl = !string.IsNullOrWhiteSpace(photoUrl) ? photoUrl : throw new ArgumentNullException(nameof(photoUrl));
            RangeHigh = !string.IsNullOrWhiteSpace(rangeHigh) ? rangeHigh : throw new ArgumentNullException(nameof(rangeHigh));
            RangeLow = !string.IsNullOrWhiteSpace(rangeLow) ? rangeLow : throw new ArgumentNullException(nameof(rangeLow));
            LowQty = lowQty > 0 ? lowQty : throw new ArgumentNullException(nameof(lowQty));
        }
        public Principle Principle { get; private set; }
        public string SKUPrinciple { get; private set; }
        public DateTime ExpireDate { get; private set; }
        public int ExpireDateLimit { get; private set; }
        public bool HasExpireDate { get; private set; }
        public string Name { get; private set; }
        public int Id { get; private set; }
        public bool HasRange { get; private set; }
        public string AuthorId { get; private set; }
        public string PhotoUrl { get; private set; }
        public string RangeHigh { get; private set; }
        public string RangeLow { get; private set; }
        public int LowQty { get; private set; }
        public int GetPrincipleId()
        {
            return _principleId;
        }
    }
}
