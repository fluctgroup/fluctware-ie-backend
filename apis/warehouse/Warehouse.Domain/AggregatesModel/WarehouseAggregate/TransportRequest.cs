﻿using Library.Abstraction;
using System;
using System.Collections.Generic;
using Warehouse.Domain.AggregatesModel.ProductAggregate;
using Warehouse.Domain.Exceptions;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public class TransportRequest:Entity
    {
        private int _statusId;
        private int _typeId;
        private int _productId;
        private readonly List<TransportRequestItem> _items;
        public IReadOnlyCollection<TransportRequestItem> Items => _items;
        public int Id { get; private set; }
        public string From { get; private set; }
        public string To { get; private set; }
        public DateTime InboundTime { get; private set; }
        public TransportType Type { get; private set; }
        public TransportRequestStatus Status { get; private set; }
        public Product Product { get; private set; }
        protected TransportRequest()
        {
            _items = new List<TransportRequestItem>();
        }
        public TransportRequest(int productId, string from, string to, DateTime inboundTime, 
            int statusId,int typeId, List<TransportRequestItem> items):this()
        {
            _productId = productId > 0 ? productId : throw new ArgumentNullException(nameof(productId));
            From = !string.IsNullOrWhiteSpace(from) ? from : throw new ArgumentNullException(nameof(from));
            To = !string.IsNullOrWhiteSpace(to) ? to : throw new ArgumentNullException(nameof(to));
            InboundTime = inboundTime;
            _statusId = statusId > 0 ? statusId : throw new ArgumentNullException(nameof(statusId));
            _typeId = typeId > 0 ? typeId : throw new ArgumentNullException(nameof(typeId));
            if (items.Count > 0)
            {
                _items.AddRange(items);
            }
            else
            {
                throw new ArgumentNullException(nameof(items));
            }
        }
        public void RejectRequest()
        {
            if (_statusId != TransportRequestStatus.WaitingApproval.Id)
            {
                throw new WarehouseDomainException("Invalid Request");
            }
            _statusId = TransportRequestStatus.Rejected.Id;
        }
        public void ApproveRequest()
        {
            if (_statusId != TransportRequestStatus.WaitingApproval.Id)
            {
                throw new WarehouseDomainException("Invalid Request");
            }
            _statusId = TransportRequestStatus.WaitingActions.Id;
        }
        public void CancelRequest()
        {
            if (_statusId != TransportRequestStatus.WaitingApproval.Id)
            {
                throw new WarehouseDomainException("Invalid Request");
            }
            _statusId = TransportRequestStatus.Cancelled.Id;
        }
        public void Delivering()
        {
            if (_statusId != TransportRequestStatus.WaitingActions.Id)
            {
                throw new WarehouseDomainException("Invalid Request");
            }
            _statusId = TransportRequestStatus.OnDelivery.Id;
        }
        public void Shipped()
        {
            if (_statusId != TransportRequestStatus.OnDelivery.Id)
            {
                throw new WarehouseDomainException("Invalid Request");
            }
            _statusId = TransportRequestStatus.Delivered.Id;
        }
        public int GetProductId()
        {
            return _productId;
        }
        public int GetStatusId()
        {
            return _statusId;
        }
    }
}
