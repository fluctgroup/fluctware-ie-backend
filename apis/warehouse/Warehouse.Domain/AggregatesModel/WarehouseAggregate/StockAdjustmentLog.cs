﻿using Library.Abstraction;
using System;
using System.Collections.Generic;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public class StockAdjustmentLog : ValueObject
    {
        public int Id { get; private set; }
        public string Note { get; private set; }
        public int StockId { get; private set; }
        public int BeforeStock { get; private set; }
        public int AfterStock { get; private set; }
        public DateTime ModifiedDate { get; private set; }
        public string ModifiedBy { get; private set; }
        public StockAdjustmentLog(string note, int stockId, string modifiedBy,int beforeStock, int afterStock)
        {
            Note = note;
            StockId = stockId;
            ModifiedBy = !string.IsNullOrWhiteSpace(modifiedBy) ? modifiedBy : throw new ArgumentNullException(nameof(modifiedBy));
            ModifiedDate = DateTime.UtcNow;
            BeforeStock = beforeStock;
            AfterStock = afterStock;
        }
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Id;
            yield return Note;
            yield return StockId;
            yield return ModifiedBy;
            yield return ModifiedDate;
            yield return BeforeStock;
            yield return AfterStock;
        }
    }
}
