using Library.Abstraction;

using W = Warehouse.Domain.AggregatesModel.WarehouseAggregate.Warehouse;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public interface IWarehouseRepository : IRepository<W>
    {
    }
}
