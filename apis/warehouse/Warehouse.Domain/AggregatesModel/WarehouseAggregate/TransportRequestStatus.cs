﻿using Library.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using Warehouse.Domain.Exceptions;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public class TransportRequestStatus : Enumeration
    {
        public static TransportRequestStatus Delivered = new TransportRequestStatus(1, nameof(Delivered).ToLowerInvariant());
        public static TransportRequestStatus OnDelivery = new TransportRequestStatus(2, nameof(OnDelivery).ToLowerInvariant());
        public static TransportRequestStatus WaitingApproval = new TransportRequestStatus(3, nameof(WaitingApproval).ToLowerInvariant());
        public static TransportRequestStatus WaitingActions = new TransportRequestStatus(4, nameof(WaitingActions).ToLowerInvariant());
        public static TransportRequestStatus Rejected = new TransportRequestStatus(5, nameof(Rejected).ToLowerInvariant());
        public static TransportRequestStatus Cancelled = new TransportRequestStatus(6, nameof(Cancelled).ToLowerInvariant());

        protected TransportRequestStatus()
        {
        }

        public TransportRequestStatus(int id, string name)
          : base(id, name)
        {
        }

        public static IEnumerable<TransportRequestStatus> List()
        {
            return new[] { Delivered, OnDelivery,WaitingApproval, WaitingActions, Rejected,Cancelled };
        }

        public static TransportRequestStatus FromName(string name)
        {
            var state = List()
              .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new WarehouseDomainException(
                  $"Possible values for TransportStatus: {string.Join(",", List().Select(s => s.Name))}");

            return state;
        }

        public static TransportRequestStatus From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new WarehouseDomainException(
                  $"Possible values for TransportStatus: {string.Join(",", List().Select(s => s.Name))}");

            return state;
        }
    }
}
