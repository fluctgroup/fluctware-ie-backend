﻿using Library.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public class Owner: Entity
    {
        public string OwnerId { get; private set; }
        public string OwnerName { get; private set; }

        public string OwnerEmail { get; private set; }
        public string OwnerPhone { get; private set; }
        public Owner(string ownerId, string ownerName, string ownerEmail, string ownerPhone)
        {
            OwnerId= !string.IsNullOrWhiteSpace(ownerId) ? ownerId : throw new ArgumentNullException(nameof(ownerId));
            OwnerName = !string.IsNullOrWhiteSpace(ownerName) ? ownerName : throw new ArgumentNullException(nameof(ownerName));
            OwnerEmail = !string.IsNullOrWhiteSpace(ownerEmail) ? ownerEmail : throw new ArgumentNullException(nameof(ownerEmail));
            OwnerPhone = !string.IsNullOrWhiteSpace(ownerPhone) ? ownerPhone : throw new ArgumentNullException(nameof(ownerPhone));
        }

    }
}
