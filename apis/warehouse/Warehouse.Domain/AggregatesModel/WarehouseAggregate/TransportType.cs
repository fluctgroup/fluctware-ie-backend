﻿using Library.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using Warehouse.Domain.Exceptions;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public class TransportType : Enumeration
    {
        public static TransportType Inbound = new TransportType(1, nameof(Inbound).ToLowerInvariant());
        public static TransportType Outbound = new TransportType(2, nameof(Outbound).ToLowerInvariant());
        public static TransportType Scrap = new TransportType(3, nameof(Scrap).ToLowerInvariant());
        public static TransportType Return = new TransportType(4, nameof(Return).ToLowerInvariant());
        protected TransportType()
        {
        }

        public TransportType(int id, string name)
          : base(id, name)
        {
        }

        public static IEnumerable<TransportType> List()
        {
            return new[] { Inbound, Outbound, Scrap,Return };
        }

        public static TransportType FromName(string name)
        {
            var state = List()
              .SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new WarehouseDomainException(
                  $"Possible values for TransportStatus: {string.Join(",", List().Select(s => s.Name))}");

            return state;
        }

        public static TransportType From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new WarehouseDomainException(
                  $"Possible values for TransportStatus: {string.Join(",", List().Select(s => s.Name))}");

            return state;
        }
    }
}
