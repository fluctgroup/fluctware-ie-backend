﻿using Library.Abstraction;
using System;
using System.Collections.Generic;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public class TransportRequestItem:ValueObject
    {
        protected TransportRequestItem()
        {

        }
        public TransportRequestItem(int requestId,string sku, DateTime expiryDate, bool unique, int qty, string quantifier):base()
        {
            RequestId = requestId;
            SKU = !string.IsNullOrWhiteSpace(sku) ? sku : throw new ArgumentNullException(nameof(sku));
            ExpiryDate = expiryDate;
            Qty = qty > 0 ? qty : throw new ArgumentNullException(nameof(qty));
            Unique = unique;
            Quantifier = !string.IsNullOrWhiteSpace(quantifier) ? quantifier : throw new ArgumentNullException(nameof(quantifier));
        }
        public int RequestId { get; private set; }
        public int Id { get; private set; }
        public string SKU { get; private set; }
        public string SKUInternal { get => $"{SKU}-{ExpiryDate}"; }
        public DateTime ExpiryDate { get; private set; }
        public bool Unique { get; private set; }
        public int Qty { get; private set; }
        public string Quantifier { get; private set; }
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return RequestId;
            yield return Id;
            yield return SKU;
            yield return ExpiryDate;
            yield return SKUInternal;
            yield return Unique;
            yield return Qty;
            yield return Quantifier;
        }
    }
}
