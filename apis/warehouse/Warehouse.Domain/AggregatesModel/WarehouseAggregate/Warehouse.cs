﻿using Library.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using Warehouse.Domain.Exceptions;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public class Warehouse : Entity, IAggregateRoot
    {
        private string _ownerId;
        private readonly List<TransportRequest> _transportRequests;
        private readonly List<Stock> _stocks;
        public Owner Owner { get; private set; }
        public IReadOnlyCollection<TransportRequest> TransportRequests => _transportRequests;
        public IReadOnlyCollection<Stock> Stocks => _stocks;
        protected Warehouse()
        {
            _transportRequests = new List<TransportRequest>();
            _stocks = new List<Stock>();
        }
        public Warehouse(string name, int spaceLeft,
            double latitude, double longitude,
            string opens, string closed, string ownerId,
            string areaLevel1, string areaLevel2,
            string areaLevel3, string areaLevel4, int freezer, int chiller)
        {

            UpdateWarehouse(ownerId, name, spaceLeft, latitude, longitude, opens, closed, areaLevel1, areaLevel2, areaLevel3, areaLevel4, freezer, chiller);
        }
        public void UpdateWarehouse(string ownerId, string name, int spaceLeft,
            double latitude, double longitude, string opens, string closed,
            string areaLevel1, string areaLevel2, string areaLevel3, string areaLevel4,
            int freezer, int chiller)
        {
            _ownerId = !string.IsNullOrWhiteSpace(ownerId) ? ownerId : throw new ArgumentNullException(nameof(ownerId));
            Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
            AreaLevel1 = areaLevel1;
            AreaLevel2 = areaLevel2;
            AreaLevel3 = areaLevel3;
            AreaLevel4 = areaLevel4;
            SpaceLeft = spaceLeft >= 0 ? spaceLeft : throw new ArgumentNullException(nameof(spaceLeft));
            Opens = !string.IsNullOrWhiteSpace(opens) ? opens : throw new ArgumentNullException(nameof(opens));
            Closed = !string.IsNullOrWhiteSpace(closed) ? closed : throw new ArgumentNullException(nameof(closed));
            Latitude = latitude;
            Longitude = longitude;
            Freezers = freezer;
            Chillers = chiller;
        }
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int SpaceLeft { get; private set; }
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
        public string Opens { get; private set; }
        public string Closed { get; private set; }
        public string AreaLevel1 { get; private set; }
        public string AreaLevel2 { get; private set; }
        public string AreaLevel3 { get; private set; }
        public string AreaLevel4 { get; private set; }
        public int Freezers { get; private set; }
        public int Chillers { get; private set; }
        public DateTime UpdatedDate { get; private set; }
        public string UpdatedBy { get; private set; }


        public string OwnerId { get => _ownerId; }
        public Stock GetStock(string sku)
        {
            return _stocks.FirstOrDefault(x => x.SKU == sku && x.Qty > 0);
        }
        public IEnumerable<Stock> GetAllStock(string sku)
        {
            return _stocks.Where(x => x.SKU == sku && x.Qty > 0).ToList();
        }
        /// <summary>
        /// Called when you ask other people to take your stocks.
        /// </summary>
        public void RequestInboundStock(int productId, string to, List<TransportRequestItem> items)
        {
            try
            {
                if (items == null || items.Count <= 0)
                {
                    throw new WarehouseDomainException("Item Empty");
                }

                var request = new TransportRequest(productId, Id.ToString(),to,DateTime.UtcNow,
                    TransportRequestStatus.WaitingApproval.Id, TransportType.Inbound.Id,
                    items);
                _transportRequests.Add(request);

            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }
        }
        public void RequestOutboundStock(int productId, string to,
            List<TransportRequestItem> items)
        {
            try
            {
                if (items == null || items.Count <= 0)
                {
                    throw new WarehouseDomainException("Item Empty");
                }
                var request = new TransportRequest(productId, Id.ToString(), to, DateTime.UtcNow,
                    TransportRequestStatus.WaitingApproval.Id, TransportType.Outbound.Id,
                    items);
                _transportRequests.Add(request);

            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }

        }
        public void CancelRequestTransport(int requestId)
        {
            try
            {
                var req = _transportRequests.SingleOrDefault(x => x.Id == requestId);
                if (req == null)
                {
                    throw new WarehouseDomainException("Request tidak ditemukan");
                }
                req.CancelRequest();
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }
        }
        /// <summary>
        /// Org lain approve utk menerima stok, selanjutny butuh action dari user utk 
        /// melakukan outbound
        /// </summary>
        /// <param name="requestId"></param>
        public void ApproveRequestInboundStock(int requestId)
        {
            try
            {
                var req = _transportRequests.SingleOrDefault(x => x.Id == requestId);
                if (req == null || req.Type.Id != TransportType.Inbound.Id)
                {
                    throw new WarehouseDomainException("Request tidak ditemukan");
                }
                req.ApproveRequest();
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }

        }
        /// <summary>
        /// Org lain approve utk menerima stok, selanjutny butuh action dari user utk 
        /// melakukan outbound
        /// </summary>
        /// <param name="requestId"></param>
        public void RejectRequestInboundStock(int requestId)
        {
            try
            {
                var req = _transportRequests.SingleOrDefault(x => x.Id == requestId);
                if (req == null || req.Type.Id != TransportType.Inbound.Id)
                {
                    throw new WarehouseDomainException("Request tidak ditemukan");
                }
                req.RejectRequest();
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }

        }
        /// <summary>
        /// Outbound Stock via request transport
        /// </summary>
        /// <param name="requestId"></param>
        public void OutboundRequestedStock(int requestId, string authorId, string notes)
        {
            try
            {
                var req = _transportRequests.SingleOrDefault(x => x.Id == requestId);
                if (req == null)
                {
                    throw new WarehouseDomainException("Request tidak ditemukan");
                }

                var stocks = req.Items.Select(y =>
                {
                    var stock = _stocks.SingleOrDefault(x => x.SKU == y.SKU && x.ExpiryDate==y.ExpiryDate);
                    if (stock == null)
                    {
                        throw new WarehouseDomainException("Stock tidak ditemukan");
                    }

                    stock.OutboundStock(authorId, y.Qty, notes);
                    return stock;
                }).ToList();

                req.Delivering();
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }
        }
        public void OutboundStock(List<TransportRequestItem> stock, string authorId, int productId, string notes,string to)
        {
            try
            {
                if (stock == null || stock.Count <= 0)
                {
                    throw new WarehouseDomainException("stock is empty.");
                }

                var request = new TransportRequest(productId, Id.ToString(),to , DateTime.UtcNow,
                    TransportRequestStatus.Delivered.Id, TransportType.Outbound.Id,
                    stock);

                _transportRequests.Add(request);

                foreach (var s in stock)
                {
                     var stoq = Stocks.SingleOrDefault(x => x.SKU == s.SKU && x.ExpiryDate==s.ExpiryDate);
                    if (stoq == null)
                    {
                        throw new WarehouseDomainException("SKU tidak ditemukan.");
                    }
                    else
                    {
                        stoq.OutboundStock(authorId, s.Qty, notes);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }
        }

        public TransportRequest InboundStockShipped(int requestId)
        {
            try
            {
                var req = _transportRequests.SingleOrDefault(x => x.Id == requestId);
                if (req == null)
                {
                    throw new WarehouseDomainException("Request tidak ditemukan");
                }
                
                req.Shipped();
                return req;
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }
        }

        /// <summary>
         /// Inbound Stock via request transport
         /// </summary>
        public void InboundRequestedStock(IEnumerable<TransportRequestItem> items,int productId, string authorId, string notes)
        {
            try
            {
                var stocks = items.Select(y =>
                {
                    var stock = _stocks.SingleOrDefault(x => x.SKU == y.SKU && x.ExpiryDate==y.ExpiryDate);
                    if (stock == null)
                    {
                        _stocks.Add(new Stock(Id, y.Qty, notes, y.SKU,
                            authorId, y.ExpiryDate, y.Quantifier, productId,
                            y.Unique));
                    }
                    else
                    {
                        stock.AddStock(authorId, y.Qty, notes);
                    }

                    return stock;
                }).ToList();

            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }
        }
        public void InboundStock(List<TransportRequestItem> stock,
            string authorId, int productId, string notes,string from)
        {
            try
            {
                if (stock == null || stock.Count<=0)
                {
                    throw new WarehouseDomainException("stock is empty.");
                }

                var request = new TransportRequest(productId, from, Id.ToString(), DateTime.UtcNow,
                    TransportRequestStatus.Delivered.Id, TransportType.Inbound.Id,
                    stock);

                _transportRequests.Add(request);

                foreach (var s in stock)
                {
                    var stoq = Stocks.SingleOrDefault(x => x.SKU == s.SKU && x.ExpiryDate==s.ExpiryDate);
                    if (stoq == null)
                    {
                        _stocks.Add(new Stock(Id, s.Qty, "", s.SKU, authorId, s.ExpiryDate,
                            s.Quantifier, productId, s.Unique));
                    }
                    else
                    {
                        stoq.AddStock(authorId, s.Qty, notes);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }
        }
        public void ApproveRequestOutboundStock(int requestId)
        {
            try
            {
                var req = _transportRequests.SingleOrDefault(x => x.Id == requestId);
                if (req == null || req.Type.Id != TransportType.Outbound.Id)
                {
                    throw new WarehouseDomainException("Request tidak ditemukan");
                }
                req.ApproveRequest();
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }

        }
        public void ReturnRequest(List<TransportRequestItem> stock, string authorId, int productId, string principle, string notes)
        {
            try
            {
                if (stock == null || stock.Count <= 0)
                {
                    throw new WarehouseDomainException("stock is empty.");
                }

                var request = new TransportRequest(productId, Id.ToString(), principle, DateTime.UtcNow,
                    TransportRequestStatus.Delivered.Id, TransportType.Return.Id,
                    stock);
                _transportRequests.Add(request);

                foreach (var s in stock)
                {
                    var stoq = Stocks.SingleOrDefault(x => x.SKU == s.SKU && x.ExpiryDate == s.ExpiryDate);
                    if (stoq == null)
                    {
                        throw new WarehouseDomainException("SKU tidak ditemukan.");
                    }
                    else
                    {
                        stoq.OutboundStock(authorId, s.Qty, notes);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }

        }
        public void RejectRequestOutboundStock(int requestId)
        {
            try
            {
                var req = _transportRequests.SingleOrDefault(x => x.Id == requestId);
                if (req == null || req.Type.Id != TransportType.Outbound.Id)
                {
                    throw new WarehouseDomainException("Request tidak ditemukan");
                }
                req.RejectRequest();
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }

        }
        public void TransportRequestRaiseProblem(int requestId)
        {
            try
            {
                var req = _transportRequests.SingleOrDefault(x => x.Id == requestId);
                if (req == null)
                {
                    throw new WarehouseDomainException("Request tidak ditemukan");
                }
                req.RejectRequest();
            }
            catch (Exception ex)
            {
                throw new WarehouseDomainException(ex.Message);
            }

        }
    }
}
