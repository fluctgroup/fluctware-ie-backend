﻿using Library.Abstraction;
using System;
using System.Collections.Generic;
using Warehouse.Domain.AggregatesModel.ProductAggregate;
using Warehouse.Domain.Exceptions;

namespace Warehouse.Domain.AggregatesModel.WarehouseAggregate
{
    public class Stock : Entity
    {
        private int _productId;
        private readonly List<StockAdjustmentLog> _stockAdjustmentLogs;
        
        public int Id { get; private set; }

        public IReadOnlyCollection<StockAdjustmentLog> StockAdjustmentLogs => _stockAdjustmentLogs;
        public int WarehouseId { get; private set; }
        public Product Product { get; private set; }
        public DateTime ExpiryDate { get; private set; }
        public DateTime InboundDate { get; private set; }
        public int Qty { get; private set; }
        public string Note { get; private set; }
        public string SKU { get; private set; }
        public string AuthorId { get; private set; }
        public string Quantifier { get; private set; }
        /// <summary>
        /// Unique Stock
        /// </summary>
        public bool Unique { get; private set; }
        protected Stock()
        {
            _stockAdjustmentLogs = new List<StockAdjustmentLog>();
        }
        public Stock(int warehouseId,int qty, string note,string sku, 
            string authorId, DateTime expiryDate, string quantifier, int productId,
            bool unique=false):this()
        {
            _productId = productId > 0 ? productId : throw new ArgumentNullException(nameof(productId));
            WarehouseId = warehouseId > 0 ? warehouseId : throw new ArgumentNullException(nameof(warehouseId));
            Note = note;
            SKU = !string.IsNullOrWhiteSpace(sku) ? sku : throw new ArgumentNullException(nameof(sku));
            AuthorId = !string.IsNullOrWhiteSpace(authorId) ? authorId : throw new ArgumentNullException(nameof(authorId));
            Quantifier = !string.IsNullOrWhiteSpace(quantifier) ? quantifier : throw new ArgumentNullException(nameof(quantifier));
            InboundDate = DateTime.UtcNow;
            ExpiryDate = expiryDate;
            Qty = qty > 0 ? qty : throw new ArgumentNullException(nameof(qty));
            Unique = unique;

            StockAdjustmentLog log = new StockAdjustmentLog(note, Id, authorId, 0, Qty);
            _stockAdjustmentLogs.Add(log);
        }
        public void EditStock(int qty, string authorId, DateTime expiryDate, string quantifier, 
            string note="", string sku="",bool unique = false)
        {
            Unique = unique;
            Note = note;
            SKU = !string.IsNullOrWhiteSpace(sku) ? sku : SKU;
            AuthorId = !string.IsNullOrWhiteSpace(authorId) ? authorId : throw new ArgumentNullException(nameof(authorId));
            ExpiryDate = expiryDate;
            Quantifier = !string.IsNullOrWhiteSpace(quantifier) ? quantifier : throw new ArgumentNullException(nameof(quantifier));
            _stockAdjustmentLogs.Add(new StockAdjustmentLog(note, Id, authorId, Qty, qty));
            Qty = qty > 0 ? qty : throw new ArgumentNullException(nameof(qty));
        }
        public void AddStock(string authorId, int qty = 1, string note = "")
        {
            int beforeQty = Qty;
            if (Unique)
            {
                if (Qty == 0)
                {
                    Qty = 1;
                }
                else
                {
                    throw new WarehouseDomainException("Stock sudah ada");
                }
            }
            else
            {
                if (qty > 0)
                {
                    Qty += qty;
                }
                else
                {
                    throw new WarehouseDomainException("Qty harus positive");
                }
            }

            StockAdjustmentLog log = new StockAdjustmentLog(note, Id, authorId, beforeQty, Qty);
            _stockAdjustmentLogs.Add(log);

        }
        public void OutboundStock(string authorId,int qty=1, string note="")
        {
            int beforeQty = Qty;
            if (Unique)
            {
                if (Qty > 0)
                {
                    Qty = 0;
                }
                else
                {
                    throw new WarehouseDomainException("Stock kurang");
                }
            }
            else
            {
                if (qty > 0)
                {
                    if (qty > Qty)
                    {
                        throw new WarehouseDomainException("Stock kurang");
                    }
                    Qty -= qty;
                }
                else
                {
                    throw new WarehouseDomainException("Qty harus positive");
                }
            }

            StockAdjustmentLog log = new StockAdjustmentLog(note, Id, authorId, beforeQty,Qty);
            _stockAdjustmentLogs.Add(log);
        }
        private bool CheckExpiryDate()
        {
            return DateTime.UtcNow < ExpiryDate;
        }
        public int GetProductId()
        {
            return _productId;
        }
    }
}
