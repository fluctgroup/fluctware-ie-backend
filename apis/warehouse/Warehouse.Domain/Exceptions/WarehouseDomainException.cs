using System;

namespace Warehouse.Domain.Exceptions
{
    /// <summary>
    ///     Exception type for app exceptions
    /// </summary>
    public class WarehouseDomainException : Exception
    {
        public WarehouseDomainException()
        {
        }

        public WarehouseDomainException(string message)
            : base(message)
        {
        }

        public WarehouseDomainException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}