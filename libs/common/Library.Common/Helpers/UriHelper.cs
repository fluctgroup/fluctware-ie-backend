using System.Linq;

namespace Library.Common.Helpers
{
    public static class UriHelper
    {
        public static string CombineUri(params string[] uriParts)
        {
            var uri = string.Empty;
            if (uriParts != null && uriParts.Any())
            {
                char[] trims = {'\\', '/'};
                uri = (uriParts[0] ?? string.Empty).TrimEnd(trims);
                for (var i = 1; i < uriParts.Count(); i++)
                    uri = $"{uri.TrimEnd(trims)}/{(uriParts[i] ?? string.Empty).TrimStart(trims)}";
            }

            return uri;
        }
    }
}