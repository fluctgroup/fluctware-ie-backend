﻿namespace Library.Common
{
    public static class ConfigurationUtility
    {
        public static string GetDbConnectionString(string sqlFormat, string instanceName, string dbUser, string dbPass)
        {
            return string.Format(sqlFormat, instanceName, dbUser, dbPass);
        }
    }
}