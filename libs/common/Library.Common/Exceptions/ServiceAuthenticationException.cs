﻿namespace Library.Common.Exceptions
{
    public class ServiceAuthenticationException : System.Exception
    {
        public ServiceAuthenticationException()
        {
        }

        public ServiceAuthenticationException(string content)
        {
            Content = content;
        }

        public string Content { get; }
    }
}
