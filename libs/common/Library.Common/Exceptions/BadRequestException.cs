﻿using System.Net.Http;

namespace Library.Common.Exceptions
{
    public class BadRequestException : HttpRequestException
    {
        public BadRequestException() : this(null, null)
        {
        }

        public BadRequestException(string message) : this(message, null)
        {
        }

        public BadRequestException(string message, System.Exception inner) : base(message,
            inner)
        {
        }
    }
}
