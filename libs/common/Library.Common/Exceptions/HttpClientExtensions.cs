using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;

namespace Library.Common.Exceptions
{
    public static class HttpClientExtensions
    {
        public static HttpClient CreateIdempotentClient(this HttpClient client)
        {
            var guid = Guid.NewGuid().ToString();
            client.DefaultRequestHeaders.Add("x-requestid", guid);
            return client;
        }

        public static async Task<string> CreateAccessToken(this HttpClient client,
            string clientId, string clientSecret, string scope, string address)
        {
            var httpClient=new HttpClient();
            var discoReq = new DiscoveryDocumentRequest()
            {
                Address = address,
                Policy = new DiscoveryPolicy()
                {
                    RequireHttps = false,
                    ValidateIssuerName = false
                }
            };
            var disco = await httpClient.GetDiscoveryDocumentAsync(discoReq);

            if (disco.IsError) throw new Exception(disco.Error);
            // request token
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,

                ClientId = clientId,
                ClientSecret = clientSecret,
                Scope = scope
            });

            if (tokenResponse.IsError) throw new Exception(tokenResponse.Error);

            return tokenResponse.AccessToken;
        }
    }
}