using System;

namespace Library.Common.Extensions
{
    public static class TimeExtensions
    {
        public static DateTime ConvertToIndoChinaTime(this DateTime utcTime)
        {
            TimeZoneInfo cstZone = null;
            try
            {
                cstZone = TimeZoneInfo.FindSystemTimeZoneById("Asia/Jakarta");
            }
            catch (TimeZoneNotFoundException)
            {
                cstZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            }

            var cstTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, cstZone);
            return cstTime;
        }
    }
}