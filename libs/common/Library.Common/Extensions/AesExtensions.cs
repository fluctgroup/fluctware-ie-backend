using Serilog;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Library.Common.Extensions
{
    public static class AesExtensions
    {
        public const int HashByteSize = 128;
        public const int Pbkdf2Iterations = 10000;

        private static byte[] GetKey(string password, string salt)
        {
            var saltByte = salt.HexStringToByte();
            var rfc2898 =
                new Rfc2898DeriveBytes(password, saltByte, Pbkdf2Iterations);
            var result = rfc2898.GetBytes(16);
            Log.Debug(Convert.ToBase64String(result));
            return result;
        }

        public static string EncryptToAes(this string cleartext, string key, string salt, string iv)
        {
            var aesCipher = new AesManaged();
            aesCipher.KeySize = HashByteSize;
            aesCipher.BlockSize = HashByteSize;
            aesCipher.Mode = CipherMode.CBC;
            aesCipher.Padding = PaddingMode.PKCS7;
            aesCipher.Key = GetKey(key, salt);
            aesCipher.IV = iv.HexStringToByte();

            var b = Encoding.UTF8.GetBytes(cleartext);
            using (var encryptTransform = aesCipher.CreateEncryptor(aesCipher.Key, aesCipher.IV))
            {
                var ctext = encryptTransform.TransformFinalBlock(b, 0, b.Length);
                return Convert.ToBase64String(ctext);
            }
        }
    }
}