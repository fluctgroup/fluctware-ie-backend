namespace Library.Common.Extensions
{
    public static class PhoneExtensions
    {
        public static string ToIdPhoneExtensions(this string phone)
        {
            phone = phone.Replace(" ", "");
            if (phone[0].ToString() == "6") phone = "+" + phone;
            else if (phone[0].ToString() == "0") phone = "+62" + phone.Substring(1);
            return phone;
        }

        public static string BackToIdPhoneExtensions(this string phone)
        {
            phone = phone.Replace("+62", "0");
            return phone;
        }
    }
}