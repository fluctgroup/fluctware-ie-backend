namespace Library.Common.Extensions
{
    public static class PinExtensions
    {
        public static string GeneratePinHash(this string pin, string sid)
        {
            var hashPin = string.Format("{0}:{1}", sid, pin).HashSha256();
            return hashPin;
        }
    }
}