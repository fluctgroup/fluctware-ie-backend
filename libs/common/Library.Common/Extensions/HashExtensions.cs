using System;
using System.Security.Cryptography;
using System.Text;

namespace Library.Common.Extensions
{
    public static class HashExtensions
    {
        /// <summary>
        ///     Creates a SHA256 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>A hash</returns>
        public static string HashSha256(this string input)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;

            using (var sha = SHA256.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var hash = sha.ComputeHash(bytes);

                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        ///     Creates a SHA256 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>A hash.</returns>
        public static byte[] HashSha256(this byte[] input)
        {
            if (input == null) return null;

            using (var sha = SHA256.Create())
            {
                return sha.ComputeHash(input);
            }
        }

        /// <summary>
        ///     Creates a SHA512 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>A hash</returns>
        public static string HashSha512(this string input)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;

            using (var sha = SHA512.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var hash = sha.ComputeHash(bytes);

                return Convert.ToBase64String(hash);
            }
        }

        /// <summary>
        ///     Same Implementation to hash_hmac PHP
        /// </summary>
        /// <param name="input"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string HashHMACSHA256PHP(this string input, string key)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;

            var keyByte = Encoding.UTF8.GetBytes(key);
            using (var sha = new HMACSHA256(keyByte))
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var hash = sha.ComputeHash(bytes);

                return hash.ByteToTwoHexadecimal();
            }
        }

        public static string HashHMACSHA256Base64(this string key, string message)
        {
            if (string.IsNullOrEmpty(message)) return "";
            var encoding = new ASCIIEncoding();

            var keyByte = encoding.GetBytes(key);
            var messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                var hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        public static string HashSHA1Base64(this string message)
        {
            if (string.IsNullOrEmpty(message)) return "";
            var encoding = new UTF8Encoding();

            var messageBytes = encoding.GetBytes(message);
            using (var sha1 = new SHA1CryptoServiceProvider())
            {
                var hashmessage = sha1.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
    }
}