using System;
using System.Text;

namespace Library.Common.Extensions
{
    public static class HexExtensions
    {
        public static string ByteToTwoHexadecimal(this byte[] buff)
        {
            var sbinary = "";
            for (var i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString("X2"); /* two hex format */
            return sbinary;
        }

        public static string StringToHexadecimal(this string str)
        {
            var buff = Encoding.UTF8.GetBytes(str);
            var hexString = buff.ByteToTwoHexadecimal();
            return hexString;
        }

        public static byte[] HexStringToByte(this string hex)
        {
            var bytesCount = hex.Length / 2;
            var bytes = new byte[bytesCount];
            for (var x = 0; x < bytesCount; ++x) bytes[x] = Convert.ToByte(hex.Substring(x * 2, 2), 16);
            return bytes;
        }
    }
}