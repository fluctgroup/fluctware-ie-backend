using System.Linq;
using Newtonsoft.Json.Linq;

namespace Library.Common.ViewModels.GoogleMaps
{
    public class GoogleReverseGeocoding
    {
        public GoogleReverseGeocoding(JObject jsonObject)
        {
            Address = (string) jsonObject["results"][0]["formatted_address"];
            Latitude = (double) jsonObject["results"][0]["geometry"]["location"]["lat"];
            Longitude = (double) jsonObject["results"][0]["geometry"]["location"]["lng"];
            var address = jsonObject["results"][0]["address_components"].ToArray();
            var addressComponents = address.Select(x =>
            {
                var types = x["types"].AsJEnumerable().Select(y => y.Value<string>()).ToArray();

                return new AddressComponent
                {
                    LongName = x["long_name"].ToString(),
                    ShortName = x["short_name"].ToString(),
                    Types = types
                };
            }).ToList();

            FeatureName
                = addressComponents.SingleOrDefault(x => x.Types.Contains("administrative_area_level_4"))?.LongName;
            Locality = addressComponents.SingleOrDefault(x => x.Types.Contains("administrative_area_level_3"))
                ?.LongName;
            SubAdminArea = addressComponents.SingleOrDefault(x => x.Types.Contains("administrative_area_level_2"))
                ?.LongName;
            AdminArea = addressComponents.SingleOrDefault(x => x.Types.Contains("administrative_area_level_1"))
                ?.LongName;
            Raw = jsonObject.ToString();
        }

        public string Address { get; }
        public double Longitude { get; }
        public double Latitude { get; }
        public string Raw { get; }
        public string AdminArea { get; set; }
        public string SubAdminArea { get; set; }
        public string FeatureName { get; set; }
        public string Locality { get; set; }

        public class AddressComponent
        {
            public string LongName { get; set; }
            public string ShortName { get; set; }
            public string[] Types { get; set; }
        }
    }
}