namespace Library.Common.ViewModels
{
    public class OTPRequestViewModel
    {
        public string Sid { get; set; }
        public string Hash { get; set; }
        public string Otp { get; set; }
        public string Phone { get; set; }
        public string OriginHash { get; set; }
    }
}