namespace Library.Common.ViewModels
{
    public class ValidationViewModel
    {
        public ValidationViewModel()
        {
        }

        public ValidationViewModel(string key, string message)
        {
            Key = key;
            Message = message;
        }

        public string Key { get; set; }
        public string Message { get; set; }
    }
}