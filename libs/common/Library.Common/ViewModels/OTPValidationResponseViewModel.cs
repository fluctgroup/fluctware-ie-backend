namespace Library.Common.ViewModels
{
    public class OTPValidationResponseViewModel
    {
        public bool Valid { get; set; }
        public string Hash { get; set; }
        public string Sid { get; set; }
        public string Message { get; set; }
    }
}