namespace Library.Common
{
    public class FileSettings
    {
        public bool UseCustomizationData { get; set; }
        public bool CloudStorageEnabled { get; set; }
        public string CloudProvider { get; set; }
        public string AWSStoragePublicKey { get; set; }
        public string AWSStorageApiKey { get; set; }
        public string BucketName { get; set; }
        public string BucketKeyFile { get; set; }
    }
}