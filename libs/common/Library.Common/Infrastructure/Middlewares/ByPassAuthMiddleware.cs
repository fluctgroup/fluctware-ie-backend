using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Library.Common.Infrastructure.Middlewares
{
    public class ByPassAuthMiddleware
    {
        private readonly RequestDelegate _next;
        private string _currentEditorId;

        public ByPassAuthMiddleware(RequestDelegate next)
        {
            _next = next;
            _currentEditorId = null;
        }


        public async Task Invoke(HttpContext context)
        {
            var path = context.Request.Path;
            if (path == "/noauth")
            {
                var editorid = context.Request.Query["editorid"];
                if (!string.IsNullOrEmpty(editorid)) _currentEditorId = editorid;
                context.Response.StatusCode = 200;
                context.Response.ContentType = "text/string";
                await context.Response.WriteAsync($"Editor set to {_currentEditorId}");
            }

            else if (path == "/noauth/reset")
            {
                _currentEditorId = null;
                context.Response.StatusCode = 200;
                context.Response.ContentType = "text/string";
                await context.Response.WriteAsync("Editor set to none. Token required for protected endpoints.");
            }
            else
            {
                var currentEditorId = _currentEditorId;

                var authHeader = context.Request.Headers["Authorization"];
                if (authHeader != StringValues.Empty)
                {
                    var header = authHeader.FirstOrDefault();
                    if (!string.IsNullOrEmpty(header) && header.StartsWith("Email ") && header.Length > "Email ".Length)
                        currentEditorId = header.Substring("Email ".Length);
                }


                if (!string.IsNullOrEmpty(currentEditorId))
                {
                    var editor = new ClaimsIdentity(new[]
                        {
                            new Claim("emails", currentEditorId),
                            new Claim("name", "Test editor"),
                            new Claim("nonce", Guid.NewGuid().ToString()),
                            new Claim("ttp://schemas.microsoft.com/identity/claims/identityprovider",
                                "ByPassAuthMiddleware"),
                            new Claim("nonce", Guid.NewGuid().ToString()),
                            new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname", "Editor"),
                            new Claim("sub", "1234"),
                            new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname", "Enterprise")
                        }
                        , "ByPassAuth");

                    context.User = new ClaimsPrincipal(editor);
                }

                await _next.Invoke(context);
            }
        }
    }
}