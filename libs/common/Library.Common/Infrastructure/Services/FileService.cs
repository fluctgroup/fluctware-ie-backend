using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Google;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Storage.v1;
using Google.Cloud.Storage.V1;
using Library.Common.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Serilog;
using Object = Google.Apis.Storage.v1.Data.Object;

namespace Library.Common.Infrastructure.Services
{
    public class FileService : IFileService
    {
        private string outputTemplate = "{Timestamp} [{Level}] {Message}{NewLine}{Exception}";

        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly FileSettings _settings;

        public FileService(IHostingEnvironment hostingEnvironment, IOptionsSnapshot<FileSettings> settings)
        {
            _hostingEnvironment = hostingEnvironment;
            _settings = settings.Value;
        }

        #region Locally

        /// <summary>
        ///     read file from hosting web root.
        /// </summary>
        /// <param name="folderName">
        ///     folder name to place this files
        /// </param>
        /// <param name="fileName">
        ///     file name with extension
        /// </param>
        /// <param name="cancellationToken">
        ///     used for cancellation Task
        /// </param>
        /// <returns>
        ///     stream file
        /// </returns>
        public async Task<byte[]> ReadFileLocallyAsync(string fileName, string folderName,
            CancellationToken cancellationToken = default)
        {
            var path = folderName + "/" + fileName;
            return await Task.Run(() => File.ReadAllBytes(path), cancellationToken);
        }

        /// <summary>
        ///     Delete file from hosting web root.
        /// </summary>
        /// <param name="folderName">
        ///     folder name to place this files
        /// </param>
        /// <param name="fileName">
        ///     file name with extension
        /// </param>
        public void DeleteFileLocally(string fileName, string folderName)
        {
            var path = Path.Combine(folderName, fileName);

            File.Delete(path);

            if (Directory.GetFiles(folderName).Length == 0) Directory.Delete(folderName);
        }


        /// <summary>
        ///     TODO: Test this manually.
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public void CleanImageFiles(string folderName)
        {
            if (Directory.Exists(folderName))
                foreach (var file in Directory.GetFiles(folderName))
                    file.Remove(0);
        }

        #endregion

        #region GCloud

        /// <summary>
        ///     Get Access Token From JSON Key Async
        /// </summary>
        /// <param name="jsonKeyFilePath">Path to your JSON Key file</param>
        /// <param name="scopes">Scopes required in access token</param>
        /// <returns>Access token as string Task</returns>
        private async Task<string> GetGCloudAccessTokenFromJSONKeyAsync(params string[] scopes)
        {
            using (var stream = new FileStream(_settings.BucketKeyFile, FileMode.Open, FileAccess.Read))
            {
                return await GoogleCredential
                    .FromStream(stream) // Loads key file  
                    .CreateScoped(scopes) // Gathers scopes requested  
                    .UnderlyingCredential // Gets the credentials  
                    .GetAccessTokenForRequestAsync(); // Gets the Access Token  
            }
        }

        public async Task UploadFileToGCloudBucket(string fileName, string folderPath, Stream fileStream,
            CancellationToken cancellationToken = default)
        {
            try
            {
                var bucketForImage = _settings.BucketName;

                var scopes = new[] {@"https://www.googleapis.com/auth/devstorage.full_control"};
                var accessToken = await GetGCloudAccessTokenFromJSONKeyAsync(scopes);

                var service = new StorageService();

                var newObject = new Object
                {
                    Bucket = bucketForImage,
                    Name = folderPath + "/" + fileName
                };

                var ext = FileHelper.GetImageMimeTypeFromImageFileExtension("." + fileName.Split('.')[1]);
                var uploadRequest = new ObjectsResource.InsertMediaUpload(service, newObject,
                        bucketForImage, fileStream, ext)
                    {OauthToken = accessToken};
                await uploadRequest.UploadAsync(cancellationToken);
            }
            catch (GoogleApiException e)
            {
                Log.Error(e, outputTemplate);
            }
            catch (Exception e)
            {
                Log.Error(e, outputTemplate);
                throw;
            }
        }

        public async Task DeleteFileGCloud(string fileName, string folderPath,
            CancellationToken cancellationToken = default)
        {
            try
            {
                var bucketForImage = _settings.BucketName;

                var scopes = new[] {@"https://www.googleapis.com/auth/devstorage.full_control"};
                var accessToken = await GetGCloudAccessTokenFromJSONKeyAsync(scopes);

                var service = new StorageService();

                var deleteRequest =
                    new ObjectsResource.DeleteRequest(service, bucketForImage, folderPath + "/" + fileName)
                        {OauthToken = accessToken};
                await deleteRequest.ExecuteAsync(cancellationToken);
            }
            catch (GoogleApiException e)
            {
                Log.Error(e, outputTemplate);
            }
            catch (Exception e)
            {
                Log.Error(e, outputTemplate);
                throw;
            }
        }

        public async Task<byte[]> ReadFileGCloud(string fileName, string folderPath,
            CancellationToken cancellationToken = default)
        {
            var stream = new MemoryStream();
            try
            {
                var scopes = new[] {@"https://www.googleapis.com/auth/devstorage.full_control"};
                var accessToken = await GetGCloudAccessTokenFromJSONKeyAsync(scopes);

                var storageClient =
                    StorageClient.Create(GoogleCredential.FromAccessToken(accessToken));

                storageClient.DownloadObject(_settings.BucketName, folderPath + "/" + fileName, stream);
                stream.Position = 0;
            }
            catch (GoogleApiException e)
            {
                Log.Error(e, outputTemplate);
            }
            catch (Exception e)
            {
                Log.Error(e, outputTemplate);
                throw;
            }

            return stream.ToArray();
        }

        #endregion

        #region AWS

        public async Task DeleteFileAws(string fileName, string folderPath,
            CancellationToken cancellationToken = default)
        {
            AWSCredentials credentials = new BasicAWSCredentials(_settings.AWSStoragePublicKey, _settings.AWSStorageApiKey);
            var bucketRegion = RegionEndpoint.APSoutheast1;
            IAmazonS3 client = new AmazonS3Client(credentials, bucketRegion);

            try
            {
                var deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = _settings.BucketName,
                    Key = folderPath + "/" + fileName
                };

                Log.Debug("Deleting an object");
                await client.DeleteObjectAsync(deleteObjectRequest, cancellationToken);
            }
            catch (AmazonS3Exception e)
            {
                Log.Debug("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Log.Debug("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
        }

        public async Task UploadFileToAwsBucket(string fileName, string folderPath, Stream fileStream,
            CancellationToken cancellationToken = default)
        {
            AWSCredentials credentials = new BasicAWSCredentials(_settings.AWSStoragePublicKey, _settings.AWSStorageApiKey);
            var bucketRegion = RegionEndpoint.APSoutheast1;
            IAmazonS3 client = new AmazonS3Client(credentials, bucketRegion);

            try
            {
                var fileTransferUtility =
                    new TransferUtility(client);

                await fileTransferUtility.UploadAsync(fileStream,
                    _settings.BucketName, folderPath + "/" + fileName, cancellationToken);
            }
            catch (AmazonS3Exception e)
            {
                Log.Debug("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Log.Debug("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
        }

        public async Task<byte[]> ReadFileAws(string fileName, string folderPath,
            CancellationToken cancellationToken = default)
        {
            AWSCredentials credentials = new BasicAWSCredentials(_settings.AWSStoragePublicKey, _settings.AWSStorageApiKey);
            var bucketRegion = RegionEndpoint.APSoutheast1;
            IAmazonS3 client = new AmazonS3Client(credentials, bucketRegion);

            var memoryStream = new MemoryStream();
            try
            {
                var request = new GetObjectRequest
                {
                    BucketName = _settings.BucketName,
                    Key = folderPath + "/" + fileName
                };
                using (var response = await client.GetObjectAsync(request, cancellationToken))
                {
                    var responseStream = response.ResponseStream;

                    await responseStream.CopyToAsync(memoryStream);
                }
            }
            catch (AmazonS3Exception e)
            {
                Log.Debug("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Log.Debug("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }

            return memoryStream.ToArray();
        }

        #endregion

        #region Central Separator

        public async Task UploadFileAsync(byte[] fileBinary, string fileName, string folderName,
            CancellationToken cancellationToken = default)
        {
            var stream = new MemoryStream(fileBinary);
            if (_settings.CloudStorageEnabled)
            {
                if (_settings.CloudProvider == "GCloud")
                    await UploadFileToGCloudBucket(fileName, folderName, stream, cancellationToken);
                else if (_settings.CloudProvider == "AWS")
                    await UploadFileToAwsBucket(fileName, folderName, stream, cancellationToken);
            }
        }

        public async Task DeleteFileAsync(string fileName, string folderName,
            CancellationToken cancellationToken = default)
        {
            if (!_settings.CloudStorageEnabled)
            {
                DeleteFileLocally(fileName, folderName);
            }
            else
            {
                if (_settings.CloudProvider == "GCloud")
                    await DeleteFileGCloud(fileName, folderName, cancellationToken);
                else if (_settings.CloudProvider == "AWS") await DeleteFileAws(fileName, folderName, cancellationToken);
            }
        }

        public async Task<byte[]> ReadFileAsync(string fileName, string folderName,
            CancellationToken cancellationToken = default)
        {
            byte[] binary = null;
            if (!_settings.CloudStorageEnabled)
            {
                binary = await ReadFileLocallyAsync(fileName, folderName, cancellationToken);
            }
            else
            {
                if (_settings.CloudProvider == "GCloud")
                    binary = await ReadFileGCloud(fileName, folderName, cancellationToken);
                else if (_settings.CloudProvider == "AWS")
                    binary = await ReadFileAws(fileName, folderName, cancellationToken);
            }

            return binary;
        }

        public async Task ModifyKeyValueEnvFile(string fileName, string folderName, string key, string value,
            CancellationToken cancellationToken = default)
        {
            string text = File.ReadAllText(folderName+"/"+fileName);
            var envs=text.Split("\r\n");

            if (envs.Any(x=>x.Contains(key + "=")))
            {
                var env = envs.Single(x => x.Contains(key + "="));
                var envValue = env.Split("=").LastOrDefault();
                text=text.Replace(envValue, value);
            }
            else
            {
                throw new Exception("This ENV Key not found");
            }
            File.WriteAllText(folderName + "/" + fileName, text);
        }
        #endregion
    }
}