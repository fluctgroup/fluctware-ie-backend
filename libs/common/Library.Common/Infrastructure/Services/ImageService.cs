using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Library.Abstraction;

namespace Library.Common.Infrastructure.Services
{
    public class ImageService<T> : IImageService<T> where T : IImage
    {
        private readonly IFileService _fileService;
        public ImageService(IFileService fileService)
        {
            _fileService = fileService ?? throw new ArgumentNullException(nameof(fileService));
        }

        public List<T> ChangeUriPlaceholder(List<T> items, string baseUri)
        {
            items.ForEach(catalogItem => { catalogItem.ImageUrl = baseUri + catalogItem.Id; });

            return items;
        }

        public T ChangeUriPlaceholder(T item, string baseUri)
        {
            item.ImageUrl = baseUri + item.Id + "/" + item.ImageName;
            return item;
        }

        /// <summary>
        ///     Used for make image url to be base64 url, single item single image
        /// </summary>
        /// <param name="item">Model has image</param>
        /// <param name="fileService">file utility helper</param>
        /// <param name="folderName">folder group name => Manufacturer, Product, Occasion</param>
        /// <param name="cancellationToken"></param>
        /// <returns>base64 format image model</returns>
        public async Task<T> GetImageBase64Url(T item, string folderName,
            CancellationToken cancellationToken = default)
        {
            var imageFileExtension = Path.GetExtension(item.ImageName);

            var buffer = await _fileService.ReadFileAsync(item.ImageName, folderName, cancellationToken);
            item.ImageBase64 = Convert.ToBase64String(buffer);
            return item;
        }

        public async Task<T> ConvertImageToBase64Url(T image,CancellationToken cancellationToken = default)
        {
            var imageFileExtension = Path.GetExtension(image.ImageName);
            var result = "";
            var data = image.Data;
            using (var ms = new MemoryStream())
            {
                await data.CopyToAsync(ms);
                var streamBytes = ms.ToArray();
                var imagebase64 = Convert.ToBase64String(streamBytes);
                result = "data:image/" + imageFileExtension.Substring(1) + ";base64," +
                         imagebase64;
                image.DataArray = streamBytes;
                image.ImageBase64 = result;
            }

            return image;
        }

        public void CropImage(string fileName, Rectangle cropRect, string outputFilePath)
        {
            var src = Image.FromFile(fileName);
            var target = new Bitmap(cropRect.Width, cropRect.Height);
            using (var g = Graphics.FromImage(target))
            {
                g.CompositingMode = CompositingMode.SourceCopy;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.DrawImage(src, new Rectangle(0, 0, target.Width, target.Height), cropRect, GraphicsUnit.Pixel);
                src.Dispose();
                g.Dispose();
            }

            var cropped = (Bitmap)target.Clone();
            cropped.Save(outputFilePath, ImageFormat.Jpeg);
            target.Dispose();
            cropped.Dispose();
        }

        public void ResizeImage(string fileName, int width, int height, string outputFilePath)
        {
            var inputImage = Image.FromFile(fileName);
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);
            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.DrawImage(inputImage, destRect, 0, 0, inputImage.Width, inputImage.Height, GraphicsUnit.Pixel);
                inputImage.Dispose();
                graphics.Dispose();
            }

            var resized = (Bitmap)destImage.Clone();
            resized.Save(outputFilePath, ImageFormat.Jpeg);
            destImage.Dispose();
            resized.Dispose();
        }

        public void DeleteImageLocally(string fileName, string folderName)
        {
            _fileService.DeleteFileLocally(fileName, folderName);
        }

        public void RotateFlip(string fileName, string outputFilePath, RotateFlipType rotateFlipType)
        {
            var bitmap = (Bitmap)Bitmap.FromFile(fileName);
            bitmap.RotateFlip(rotateFlipType);
            var flipped = (Bitmap)bitmap.Clone();
            flipped.Save(outputFilePath, ImageFormat.Jpeg);
            bitmap.Dispose();
            flipped.Dispose();
        }
    }
}