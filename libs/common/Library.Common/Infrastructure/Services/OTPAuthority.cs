using System;
using Library.Common.Extensions;
using Library.Common.ViewModels;

namespace Library.Common.Infrastructure.Services
{
    public class OTPAuthority : IOTPAuthority
    {
        public OTPRequestViewModel GenerateOTPClaims(string phone, bool isTesterRole, out string otp)
        {
            if (string.IsNullOrEmpty(phone)) throw new ArgumentNullException();

            var digit = 4;
            if (isTesterRole)
                otp = "1111";
            else
                otp = new Random().Next(0, (int) Math.Pow(10, digit) - 1).ToString("D4");
            var sid = DateTime.Now.Ticks.ToString();

            var hash = string.Format("{0}:{1}:{2}", sid, otp, phone).HashSha256();
            return new OTPRequestViewModel
            {
                Hash = hash,
                Sid = sid
            };
        }

        public OTPRequestViewModel GenerateOTPEmail(string originSid, string token, string originHash, string phone,
            bool isTesterRole, out string otp)
        {
            #region Validate Hash

            var encryptedHash = string.Format("{0}:{1}:{2}", originSid, token, phone).HashSha256();

            if (encryptedHash != originHash) throw new Exception("Invalid Hash");
            var sidOri = Convert.ToInt64(originSid);
            var now = DateTime.Now.Ticks;
            var difference = now - sidOri;
            if (difference > TimeSpan.FromMinutes(5).Ticks) throw new Exception("Hash Expired");

            #endregion

            var digit = 4;

            if (isTesterRole)
                otp = "1111";
            else
                otp = new Random().Next(0, (int) Math.Pow(10, digit) - 1).ToString("D4");

            var sid = DateTime.Now.Ticks.ToString();

            var hash = string.Format("{0}:{1}:{2}", sid, otp, encryptedHash).HashSha256();
            return new OTPRequestViewModel
            {
                Hash = hash,
                Sid = sid,
                OriginHash = encryptedHash
            };
        }

        public OTPValidationResponseViewModel OnVerify(OTPRequestViewModel viewmodel, string otp)
        {
            var valid = false;
            var otpId = viewmodel.Sid;
            var hash = viewmodel.Hash;
            var phone = viewmodel.Phone;

            if (string.Format("{0}:{1}:{2}", otpId, otp, phone).HashSha256() == hash)
            {
                var sid = Convert.ToInt64(otpId);
                var now = DateTime.Now.Ticks;
                var difference = now - sid;
                var message = "";
                valid = true;

                if (difference > TimeSpan.FromMinutes(5).Ticks)
                {
                    valid = false;
                    message = "OTP Expired";
                }

                return new OTPValidationResponseViewModel
                {
                    Valid = valid,
                    Hash = hash,
                    Sid = otpId,
                    Message = message
                };
            }

            throw new ArgumentException("Invalid Token");
        }

        public OTPValidationResponseViewModel OnVerifyEmail(OTPRequestViewModel viewmodel, string otp)
        {
            var valid = false;
            var otpId = viewmodel.Sid;
            var hash = viewmodel.Hash;
            var originHash = viewmodel.OriginHash;

            if (string.Format("{0}:{1}:{2}", otpId, otp, originHash).HashSha256() == hash)
            {
                var sid = Convert.ToInt64(otpId);
                var now = DateTime.Now.Ticks;
                var difference = now - sid;
                var message = "";
                valid = true;

                if (difference > TimeSpan.FromMinutes(15).Ticks)
                {
                    valid = false;
                    message = "OTP Expired";
                }

                return new OTPValidationResponseViewModel
                {
                    Valid = valid,
                    Hash = hash,
                    Sid = otpId,
                    Message = message
                };
            }

            throw new ArgumentException("Invalid Token");
        }
    }
}