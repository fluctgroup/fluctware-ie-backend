using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Library.Common.Exceptions;
using Newtonsoft.Json;

namespace Library.Common.Infrastructure.Services
{
    public class RequestProvider : IRequestProvider
    {
        private readonly HttpClient _httpClient;
        public RequestProvider(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }


        private async Task HandleResponse(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.Forbidden ||
                    response.StatusCode == HttpStatusCode.Unauthorized)
                    throw new ServiceAuthenticationException(content);

                else if (response.StatusCode == HttpStatusCode.BadRequest)
                    throw new BadRequestException(content);

                throw new HttpRequestExceptionEx(response.StatusCode, content);
            }
        }
        public async Task<string> ResetAccessToken(string clientId, string secret, string scope, string uri)
        {
            return await _httpClient.CreateAccessToken(clientId, secret, scope, uri);
        }

        public async Task<T> PostAsync<T>(string endPoints, object request, string token) where T : class
        {
            _httpClient.SetBearerToken(token);
            return await PostAsync<T>(endPoints, request);
        }
        public async Task PostAsync(string endPoints, object request, string token)
        {
            _httpClient.SetBearerToken(token);
            await PostAsync(endPoints, request);
        }
        public async Task<T> GetAsync<T>(string endPoints, string token) where T : class
        {
            _httpClient.SetBearerToken(token);
            return await GetAsync<T>(endPoints);
        }
        public async Task<T> PostAsync<T>(string endPoints, object request) where T : class
        {
            var content = new StringContent(JsonConvert.SerializeObject(request));
            content.Headers.ContentType.MediaType = "application/json";
            content.Headers.ContentType.CharSet = "";

            var response = await _httpClient.PostAsync(endPoints, content);
            await HandleResponse(response);

            var data = await response.Content.ReadAsStringAsync();

            var items = !string.IsNullOrEmpty(data) ? JsonConvert.DeserializeObject<T>(data) : null;
            return items;
        }
        public async Task PostAsync(string endPoints, object request)
        {
            var content = new StringContent(JsonConvert.SerializeObject(request));
            content.Headers.ContentType.MediaType = "application/json";
            content.Headers.ContentType.CharSet = "";

            var response = await _httpClient.PostAsync(endPoints, content);
            await HandleResponse(response);
        }
        public async Task<T> GetAsync<T>(string endPoints) where T : class
        {
            var response = await _httpClient.GetAsync(endPoints);
            await HandleResponse(response);
            var data = await response.Content.ReadAsStringAsync();

            var items = !string.IsNullOrEmpty(data) ? JsonConvert.DeserializeObject<T>(data) : null;
            return items;
        }
    }
}