using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Common.Infrastructure.Services
{
    public interface IFileService
    {
        Task ModifyKeyValueEnvFile(string fileName, string folderName,string key,string value,
            CancellationToken cancellationToken = default);
        Task UploadFileAsync(byte[] fileBinary, string fileName, string folderName,
            CancellationToken cancellationToken = default);

        Task DeleteFileAsync(string fileName, string folderName, CancellationToken cancellationToken = default);
        Task<byte[]> ReadFileAsync(string fileName, string filePath, CancellationToken cancellationToken = default);

        #region GCloud

        Task UploadFileToGCloudBucket(string fileName, string folderPath, Stream fileStream,
            CancellationToken cancellationToken = default);

        Task DeleteFileGCloud(string fileName, string folderPath, CancellationToken cancellationToken = default);
        Task<byte[]> ReadFileGCloud(string fileName, string folderPath, CancellationToken cancellationToken = default);

        #endregion

        #region AWS

        Task DeleteFileAws(string fileName, string folderPath, CancellationToken cancellationToken = default);

        Task UploadFileToAwsBucket(string fileName, string folderPath, Stream fileStream,
            CancellationToken cancellationToken = default);

        Task<byte[]> ReadFileAws(string fileName, string folderPath, CancellationToken cancellationToken = default);

        #endregion

        #region Locally

        Task<byte[]> ReadFileLocallyAsync(string fileName, string folderName,
            CancellationToken cancellationToken = default);

        void DeleteFileLocally(string fileName, string folderName);

        #endregion
    }
}