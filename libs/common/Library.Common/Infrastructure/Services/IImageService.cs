using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Common.Infrastructure.Services
{
    public interface IImageService<T>
    {
        List<T> ChangeUriPlaceholder(List<T> items, string baseUri);
        T ChangeUriPlaceholder(T item, string baseUri);
        /// <summary>
        ///     Used for make image url to be base64 url, single item single image
        /// </summary>
        /// <param name="item">Model has image</param>
        /// <param name="fileService">file utility helper</param>
        /// <param name="folderName">folder group name => Manufacturer, Product, Occasion</param>
        /// <param name="cancellationToken"></param>
        /// <returns>base64 format image model</returns>
        Task<T> GetImageBase64Url(T item, string folderName,CancellationToken cancellationToken = default);
        Task<T> ConvertImageToBase64Url(T image,CancellationToken cancellationToken = default);
        void CropImage(string fileName, Rectangle cropRect, string outputFilePath);
        void ResizeImage(string fileName, int width, int height, string outputFilePath);
        void DeleteImageLocally(string fileName,string folderName);
        void RotateFlip(string fileName, string outputFilePath, RotateFlipType rotateFlipType);

    }
}