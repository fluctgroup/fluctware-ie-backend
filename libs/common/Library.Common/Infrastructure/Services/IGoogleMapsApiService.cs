using System.Threading.Tasks;
using Library.Common.ViewModels.GoogleMaps;

namespace Library.Common.Infrastructure.Services
{
    public interface IGoogleMapsApiService
    {
        Task<GoogleDirection> GetDirections(string originLatitude, string originLongitude, string destinationLatitude,
            string destinationLongitude, string apiKey);

        Task<GooglePlaceAutoCompleteResult> GetPlaces(string text, string latitude, string longitude, string apiKey);
        Task<GooglePlace> GetPlaceDetails(string placeId, string apiKey);
        Task<GoogleReverseGeocoding> ReverseGeocoding(string latitude, string longitude, string apiKey);
    }
}