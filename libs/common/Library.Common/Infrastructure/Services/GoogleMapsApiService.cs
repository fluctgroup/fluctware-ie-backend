using System;
using System.Net.Http;
using System.Threading.Tasks;
using Library.Common.Helpers;
using Library.Common.ViewModels.GoogleMaps;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Library.Common.Infrastructure.Services
{
    public class GoogleMapsApiService : IGoogleMapsApiService
    {
        private const string ApiUrlBase = "https://maps.googleapis.com/maps/";

        private readonly HttpClient _httpClient;

        public GoogleMapsApiService(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<GoogleDirection> GetDirections(string originLatitude, string originLongitude,
            string destinationLatitude, string destinationLongitude, string apiKey)
        {
            var uri = UriHelper.CombineUri(ApiUrlBase,
                $"api/directions/json?mode=driving&transit_routing_preference=less_driving&origin={originLatitude},{originLongitude}&destination={destinationLatitude},{destinationLongitude}&key={apiKey}");

            var response = await _httpClient.GetAsync(uri);

            var data = await response.Content.ReadAsStringAsync();

            var googleDirection = JsonConvert.DeserializeObject<GoogleDirection>(data);

            return googleDirection;
        }

        public async Task<GooglePlaceAutoCompleteResult> GetPlaces(string text, string latitude, string longitude,
            string apiKey)
        {
            var uri = UriHelper.CombineUri(ApiUrlBase,
                $"api/place/autocomplete/json?input={Uri.EscapeUriString(text)}&key={apiKey}&location={latitude + "," + longitude}&radius=16000");

            var response = await _httpClient.GetAsync(uri);

            var data = await response.Content.ReadAsStringAsync();

            var googlePlaceAutoCompleteResult = JsonConvert.DeserializeObject<GooglePlaceAutoCompleteResult>(data);

            return googlePlaceAutoCompleteResult;
        }

        public async Task<GooglePlace> GetPlaceDetails(string placeId, string apiKey)
        {
            var uri = UriHelper.CombineUri(ApiUrlBase,
                $"api/place/details/json?placeid={Uri.EscapeUriString(placeId)}&key={apiKey}");

            var response = await _httpClient.GetAsync(uri);

            var jObject = await response.Content.ReadAsStringAsync();
            var result = new GooglePlace(JObject.Parse(jObject));

            return result;
        }


        public async Task<GoogleReverseGeocoding> ReverseGeocoding(string latitude, string longitude, string apiKey)
        {
            var uri = UriHelper.CombineUri(ApiUrlBase,
                $"api/geocode/json?latlng={latitude},{longitude}&key={apiKey}");

            var response = await _httpClient.GetAsync(uri);

            var jObject = await response.Content.ReadAsStringAsync();
            var result = new GoogleReverseGeocoding(JObject.Parse(jObject));

            return result;
        }
    }
}