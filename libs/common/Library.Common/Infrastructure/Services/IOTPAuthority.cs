using Library.Common.ViewModels;

namespace Library.Common.Infrastructure.Services
{
    public interface IOTPAuthority
    {
        OTPRequestViewModel GenerateOTPClaims(string phone, bool isTesterRole, out string otp);

        OTPRequestViewModel GenerateOTPEmail(string originSid, string token, string originHash, string phone,
            bool isTesterRole, out string otp);

        OTPValidationResponseViewModel OnVerify(OTPRequestViewModel viewmodel, string otp);
        OTPValidationResponseViewModel OnVerifyEmail(OTPRequestViewModel viewmodel, string otp);
    }
}