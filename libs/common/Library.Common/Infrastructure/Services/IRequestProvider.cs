using System.Threading.Tasks;

namespace Library.Common.Infrastructure.Services
{
    public interface IRequestProvider
    {
        Task<string> ResetAccessToken(string clientId, string secret, string scope, string uri);
        Task<T> PostAsync<T>(string endPoints, object request, string token) where T : class;
        Task PostAsync(string endPoints, object request, string token);
        Task<T> PostAsync<T>(string endPoints, object request) where T : class;
        Task PostAsync(string endPoints, object request);
        Task<T> GetAsync<T>(string endPoints) where T : class;
        Task<T> GetAsync<T>(string endPoints, string token) where T : class;
    }
}