using System.IO;

namespace Library.Abstraction
{
    /// <summary>
    ///     All entity that has Image on it should implement this interface
    /// </summary>
    public interface IImage
    {
        int Id { get; set; }

        string ImageUrl { get; set; }

        string ImageBase64 { get; set; }

        /// <summary>
        ///     Image Name with extension.
        /// </summary>
        string ImageName { get; set; }

        Stream Data { get; set; }
        byte[] DataArray { get; set; }
    }
}