using System.IO;

namespace Library.Abstraction
{
    public class ImageBase : IImage
    {
        public int Id { get; set; }
        /// <summary>
        /// Image Path => Local or http
        /// </summary>
        public string ImageUrl { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageName { get; set; }

        public Stream Data { get; set; }
        public byte[] DataArray { get; set; }
    }
}