using Xunit;

namespace Library.UnitTests.Common
{
    public class ExtensionsTests
    {
        [Fact]
        public void HashHMACSHA256PHP_Success()
        {
            // Arrange
            var expected = "F786EE6C3CB28C549E823B2ED42C4EB00FE7117E47DA94DC7603A10D314E1EC8";
            var str = "08128628884420181127182011getToken20181234567890";
            //var key = "fake".StringToHexadecimal();

            //// Act
            //var result = str.HashHMACSHA256PHP(key);

            //// Assert
            //Assert.Equal(expected, result);
        }

        [Fact]
        public void StringToHexadecimal_Success()
        {
            // Arrange
            var expected = "66616B65";
            var str = "fake";

            //// Act
            //var result = str.StringToHexadecimal();

            //// Assert
            //Assert.Equal(expected, result);
        }
    }
}