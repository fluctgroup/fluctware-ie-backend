﻿using System.Data.Common;
using System.Threading.Tasks;
using Library.EventBus.Events;

namespace Library.IntegrationEventLog.Services
{
    public interface IIntegrationEventLogService
    {
        Task SaveEventAsync(IntegrationEvent command, DbTransaction transaction);
        Task MarkEventAsPublishedAsync(IntegrationEvent command);
    }
}