﻿using System;
using Library.EventBus.Events;
using Newtonsoft.Json;

namespace Library.IntegrationEventLog
{
    /// <summary>
    ///     Integration Event Entity
    /// </summary>
    public class IntegrationEventLogEntry
    {
        public IntegrationEventLogEntry()
        {
        }

        public IntegrationEventLogEntry(IntegrationEvent command)
        {
            EventId = command.Id;
            CreationTime = command.CreationDate;
            EventTypeName = command.GetType().FullName;
            Content = JsonConvert.SerializeObject(command);
            State = EventStateEnum.NotPublished;
            TimesSent = 0;
        }

        public Guid EventId { get; set; }
        public string EventTypeName { get; set; }
        public EventStateEnum State { get; set; }
        public int TimesSent { get; set; }
        public DateTime CreationTime { get; set; }
        public string Content { get; set; }
    }
}